//
//  WJBaseVC.m
//  画图demo
//
//  Created by 文杰 许                       on 2018/3/22.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WJBaseVC.h"

@interface WJBaseVC ()

@end

@implementation WJBaseVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:self.imageView];
}

- (UIImageView *)imageView
{
    if (!_imageView)
    {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 150, 80, 300, 200)];
        _imageView.image = [UIImage imageNamed:@"ruhua.jpg"];
    }
    return _imageView;
}

@end
