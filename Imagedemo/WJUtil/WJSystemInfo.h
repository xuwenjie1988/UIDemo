//
//  WJSystemInfo.h
//  Imagedemo
//
//  Created by 文杰 许                       on 2018/4/4.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SYSTEM_LANGUAGE_EN 0
#define SYSTEM_LANGUAGE_CH 1
#define SYSTEM_LANGUAGE_CHT 2
#define SYSTEM_LANGUAGE_JA 3
#define SYSTEM_LANGUAGE_OTHER 4

@interface WJSystemInfo : NSObject
#pragma mark - 属性

///手机序列号
@property(nonatomic,copy,readonly) NSString *imei;

///标示号
@property(nonatomic,copy,readonly) NSString *uuid;

///手机别名
@property(nonatomic,copy,readonly) NSString *phoneName;

///设备名称
@property(nonatomic,copy,readonly) NSString *systemName;

///系统版本
@property(nonatomic,copy,readonly) NSString *osVersion;

///手机型号
@property(nonatomic,copy,readonly) NSString *phoneModel;

///手机国际化型号
@property(nonatomic,copy,readonly) NSString *localModel;

///应用名称
@property(nonatomic,copy,readonly) NSString *appName;

///应用版本
@property(nonatomic,copy,readonly) NSString *appVersion;

///应用路径
@property(nonatomic,copy,readonly) NSString *appPath;

///主目录
@property(nonatomic,copy,readonly) NSString *homePath;

///文档目录
@property(nonatomic,copy,readonly) NSString *documentsPath;

///缓存目录
@property(nonatomic,copy,readonly) NSString *cachesPath;

///临时目录
@property(nonatomic,copy,readonly) NSString *tmpPath;

///语言
@property(nonatomic,copy,readonly) NSString *language;

///语言ID
@property(nonatomic,assign,readonly) int languageID;

///地区
@property(nonatomic,copy,readonly) NSString *locale;

///应用版本数字码
@property(nonatomic,assign,readonly) int appVersionNum;


#pragma mark - 类方法

///获取实例，初始化系统参数(单例)
+ (instancetype)systemInfo;

#pragma mark - 对象方法

///描述
- (NSString *)description;
@end
