//
//  WJAppUtil.h
//  Imagedemo
//
//  Created by 文杰 许                       on 2018/4/4.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import <AVFoundation/AVFoundation.h>
#import "WJSystemInfo.h"

@interface WJAppUtil : NSObject
#pragma -mark 类方法

///发短信
+ (void)messageTo:(NSString *)number;

///打电话
+ (void)dialTo:(NSString *)number;

///发送邮件
+ (void)mailTo:(NSString *)address;

///打开URL(网页或地图)
+ (void)browseURL:(NSString *)URL;

///打开应用商店
+ (void)viewApp:(NSString *)appID;

///打开应用评分
+ (void)rateApp:(NSString *)appID;

///设置app
+ (void)goToAppSetting;

///设置iCloud(无效)
+ (void)goToCloudSetting;

///设置定位(无效)
+ (void)goToLocationSetting;

///设置通知(无效)
+ (void)goToNotificationSetting;

///设置Wi-Fi(无效)
+ (void)goToWifiSetting;

///打开app
+ (void)openApp:(NSString *)app withMessage:(NSString *)msg;

///打开URL
+ (void)openURL:(NSString *)URL;

///获取主窗口
+ (UIWindow *)keyWindow;

///获取根视图
+ (UIView *)rootViewFromView:(UIView *)view;

///获取当前的视图控制器
+ (UIViewController *)currentViewController;

///获取顶层的视图控制器
+ (UIViewController *)topViewController;

///获取view所在的视图控制器
+ (UIViewController *)controllerWithView:(UIView *)view;

///获取View相对keyWindow的frame
+ (CGRect)frameInWindowFromView:(UIView *)view;

///获取View相对于某个控件的frame
+ (CGRect)frameInOther:(UIView *)other fromView:(UIView *)view;

///获取屏幕尺寸
+ (CGSize)screenSize;

///获取屏幕缩放比率
+ (float)screenScale;

///获取状态栏尺寸
+ (CGSize)statusBarSize;

///设置网络活动指示器
+ (void)setNetworkStateActive:(BOOL)active;

///设置状态栏隐藏
+ (void)setStatusBarHidden:(BOOL)hidden withAnimation:(BOOL)animation;

///设置状态栏亮色
+ (void)setStatusBarLightStyle;

///设置状态栏暗色
+ (void)setStatusBarDarkStyle;

///获取状态栏是否亮色
+ (BOOL)isStatusBarLightStyle;

///显示状态栏
+ (void)showStatusBar;

///隐藏状态栏
+ (void)hideStatusBar;

///获取app路径
+ (NSString *)appPath;

///获取文档路径
+ (NSString *)docPath;

///获取临时文件路径
+ (NSString *)tempPath;

///获取启动图片(根据方向自动判断)
+ (UIImage *)launchImage;

///获取竖屏方向的启动图片
+ (UIImage *)portraitLaunchImage;

///获取横屏方向的启动图片
+ (UIImage *)landscapeLaunchImage;

///获取系统版本
+ (NSString *)OSVersion;

///获取app版本
+ (NSString *)appVersion;

///获取系统语言
+ (int)language;

///修复无法播放声音的问题
+ (void)repairAudioPlayer;

///设置应用图标徽标数字
+ (void)setBadgeNumber:(long)badge;

///获取当前应用图标徽标数字
+ (long)getBadgeNumber;

///设置屏幕方向
+ (BOOL)setOrientation:(UIInterfaceOrientation)orientation;

///获取当前的屏幕方向
+ (UIDeviceOrientation)currentOrientation;

///设置禁止自动锁屏
+ (void)setAutoLockScreenDisabled:(BOOL)disabled;
@end
