//
//  WJSystemInfo.m
//  Imagedemo
//
//  Created by 文杰 许                       on 2018/4/4.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WJSystemInfo.h"

@interface WJSystemInfo()
#pragma mark - 属性

///手机序列号
@property(nonatomic,copy) NSString *imei;

///标示号
@property(nonatomic,copy) NSString *uuid;

///手机别名
@property(nonatomic,copy) NSString *phoneName;

///设备名称
@property(nonatomic,copy) NSString *systemName;

///系统版本
@property(nonatomic,copy) NSString *osVersion;

///手机型号
@property(nonatomic,copy) NSString *phoneModel;

///手机国际化型号
@property(nonatomic,copy) NSString *localModel;

///应用名称
@property(nonatomic,copy) NSString *appName;

///应用版本
@property(nonatomic,copy) NSString *appVersion;

///应用路径
@property(nonatomic,copy) NSString *appPath;

///主目录
@property(nonatomic,copy) NSString *homePath;

///文档目录
@property(nonatomic,copy) NSString *documentsPath;

///缓存目录
@property(nonatomic,copy) NSString *cachesPath;

///临时目录
@property(nonatomic,copy) NSString *tmpPath;

///语言
@property(nonatomic,copy) NSString *language;

///语言ID
@property(nonatomic,assign) int languageID;

///地区
@property(nonatomic,copy) NSString *locale;

///应用版本数字码
@property(nonatomic,assign) int appVersionNum;
@end

@implementation WJSystemInfo
//单例
static  WJSystemInfo *_info=nil;


//获取实例，初始化系统参数
+ (instancetype)systemInfo
{
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^
                  {
                      _info=[[self alloc] init];
                      
                      //    info.imei=[[UIDevice currentDevice] uniqueIdentifier];
                      _info.uuid=[[UIDevice currentDevice].identifierForVendor UUIDString];
                      _info.phoneName=[[UIDevice currentDevice] name];
                      _info.systemName=[[UIDevice currentDevice] systemName];
                      _info.osVersion=[[UIDevice currentDevice] systemVersion];
                      _info.phoneModel=[[UIDevice currentDevice] model];
                      _info.localModel=[[UIDevice currentDevice] localizedModel];
                      
                      NSDictionary *infoDictionary=[[NSBundle mainBundle] infoDictionary];
                      
                      _info.appName=[infoDictionary objectForKey:@"CFBundleDisplayName"];
                      _info.appVersion=[infoDictionary objectForKey:@"CFBundleShortVersionString"];
                      _info.appVersionNum=(int)[infoDictionary objectForKey:@"CFBundleVersion"];
                      
                      _info.appPath=[[NSBundle mainBundle] bundlePath];
                      _info.homePath=NSHomeDirectory();
                      _info.documentsPath=[NSString stringWithFormat:@"%@/Documents",_info.homePath];
                      _info.cachesPath=[NSString stringWithFormat:@"%@/Library/Caches",_info.homePath];
                      _info.tmpPath=[NSString stringWithFormat:@"%@/tmp",_info.homePath];
                      
                      
                      //简体中文：zh-Hans
                      //繁体中文：zh-Hant
                      //英语：en
                      _info.language=[[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                      _info.locale=[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLocale"];
                      
                      if([_info.language hasPrefix:@"zh-Hans"])
                      {
                          _info.languageID=SYSTEM_LANGUAGE_CH;
                      }
                      else if([_info.language hasPrefix:@"zh-Hant"])
                      {
                          _info.languageID=SYSTEM_LANGUAGE_CHT;
                      }
                      else if([_info.language hasPrefix:@"en"])
                      {
                          _info.languageID=SYSTEM_LANGUAGE_EN;
                      }
                      else if([_info.language hasPrefix:@"ja"])
                      {
                          _info.languageID=SYSTEM_LANGUAGE_JA;
                      }
                      else
                      {
                          _info.languageID=SYSTEM_LANGUAGE_OTHER;
                      }
                  });
    
    
    return _info;
}


//重写alloc
+ (instancetype)alloc
{
    if(_info)
    {
        return nil;
    }
    else
    {
        return [super alloc];
    }
}

//描述
- (NSString *)description
{
    NSMutableString *ms=[NSMutableString stringWithFormat:@"KATSystemInfo:\n{\n"];
    
    [ms appendFormat:@"   [IEMI] %@ \n",_imei];
    [ms appendFormat:@"   [UUID] %@ \n",_uuid];
    [ms appendFormat:@"   [PhoneName] %@ \n",_phoneName];
    [ms appendFormat:@"   [SystemName] %@ \n",_systemName];
    [ms appendFormat:@"   [OSVersion] %@ \n",_osVersion];
    [ms appendFormat:@"   [PhoneModel] %@ \n",_phoneModel];
    [ms appendFormat:@"   [LocalModel] %@ \n",_localModel];
    [ms appendFormat:@"   [AppName] %@ \n",_appName];
    [ms appendFormat:@"   [AppVersion] %@ \n",_appVersion];
    [ms appendFormat:@"   [Language] %@ \n",_language];
    [ms appendFormat:@"   [Locale] %@ \n",_locale];
    [ms appendFormat:@"   [AppNum] %i \n",_appVersionNum];
    [ms appendFormat:@"   [AppPath] %@ \n",_appPath];
    [ms appendFormat:@"   [HomePath] %@ \n",_homePath];
    [ms appendString:@"}"];
    
    return ms;
}

@end
