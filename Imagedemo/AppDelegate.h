//
//  AppDelegate.h
//  画图demo
//
//  Created by 文杰 许                       on 2018/3/21.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

