//
//  WJColor.m
//  Imagedemo
//
//  Created by 文杰 许                       on 2018/4/2.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WJColor.h"

@implementation WJColor
//通过HSBA获取颜色(前2~3位为色相，取值范围为0~359度，之后2位为饱和度，取值范围为0~99,最后2位为透明度，取值范围为0~99)
+ (UIColor *)colorWithHSBA:(int)HSBA
{
    CGFloat h=0,s=0,b=0,a=0;
    
    h=(HSBA/COLOR_HSBA_H%360)/360.0;
    s=(HSBA%COLOR_HSBA_H/COLOR_HSBA_S)/99.0;
    b=(HSBA%COLOR_HSBA_S/COLOR_HSBA_B)/99.0;
    a=(HSBA%COLOR_HSBA_B/COLOR_HSBA_A)/99.0;
    
    return [UIColor colorWithHue:h saturation:s brightness:b alpha:a];
}

//通过RGBA字符串获取颜色
+ (UIColor *)colorWithRGBA:(NSString *)RGBA
{
    if(RGBA && RGBA.length==8)//8位数
    {
        const char *rgba=[RGBA UTF8String];
        
        if(strlen(rgba)==8)
        {
            int r=0;//红
            int g=0;//绿
            int b=0;//蓝
            int a=0;//透明
            
            for(int i=0;i<8 ;i++)
            {
                char ch=rgba[i];
                
                int value=-1;//数值
                
                if(ch>='0' && ch<='9')//数字
                {
                    value=ch-'0';
                }
                else if(ch>='A' && ch<='F')//大写字母
                {
                    value=ch-'A'+10;
                }
                else if(ch>='a' && ch<='z')//小写字母
                {
                    value=ch-'a'+10;
                }
                else//其他内容
                {
                    return nil;
                }
                
                if(i%2==0)//十位数
                {
                    value*=16;
                }
                
                if(i/2==0)//R
                {
                    r+=value;
                }
                else if(i/2==1)//G
                {
                    g+=value;
                }
                else if(i/2==2)//B
                {
                    b+=value;
                }
                else if(i/2==3)//A
                {
                    a+=value;
                }
            }
            
            return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a/255.0];
        }
    }
    
    return nil;
}


//通过RGB字符串获取颜色
+ (UIColor *)colorWithRGB:(NSString *)RGB
{
    return [WJColor colorWithRGBA:[NSString stringWithFormat:@"%@FF",RGB]];
}

@end
