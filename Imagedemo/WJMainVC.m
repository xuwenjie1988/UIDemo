//
//  WJMainVC.m
//  画图demo
//
//  Created by 文杰 许                       on 2018/3/22.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WJMainVC.h"

@interface WJMainVC ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) CAShapeLayer *eyeLayer1;
@property (nonatomic, strong) CAShapeLayer *eyeLayer2;
@property (nonatomic, strong) CAShapeLayer *eyeLayer3;
@property (nonatomic, strong) CAShapeLayer *eyeLayer4;
@property (nonatomic, strong) CAShapeLayer *eyeLayer5;
@property (nonatomic, strong) UIView *headView;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WJMainVC
{
    NSArray *lists;
}

- (CAShapeLayer *)eyeLayer1
{
    if (!_eyeLayer1)
    {
        _eyeLayer1 = [CAShapeLayer layer];
        CGPoint point = CGPointMake(CGRectGetWidth(self.headView.frame) / 2, CGRectGetHeight(self.headView.frame) / 2);
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:point
                                                            radius:CGRectGetWidth(self.headView.frame) * 0.2
                                                        startAngle:(230.f / 180.f) * M_PI
                                                          endAngle:(265.f / 180.f) * M_PI
                                                         clockwise:YES];
        _eyeLayer1.borderColor = [UIColor blackColor].CGColor;
        _eyeLayer1.strokeColor = [UIColor whiteColor].CGColor;
        _eyeLayer1.fillColor = [UIColor clearColor].CGColor;
        _eyeLayer5.lineWidth = 5.f;
        _eyeLayer1.path = path.CGPath;
    }
    return _eyeLayer1;
}

- (CAShapeLayer *)eyeLayer2
{
    if (!_eyeLayer2)
    {
        _eyeLayer2 = [CAShapeLayer layer];
        CGPoint point = CGPointMake(CGRectGetWidth(self.headView.frame) / 2, CGRectGetHeight(self.headView.frame) / 2);
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:point radius:CGRectGetWidth(self.headView.frame) * 0.2 startAngle:(211.f / 180.f) * M_PI endAngle:(220.f / 180.f) * M_PI clockwise:YES];
        _eyeLayer2.borderColor = [UIColor blackColor].CGColor;
        _eyeLayer2.strokeColor = [UIColor whiteColor].CGColor;
        _eyeLayer2.fillColor = [UIColor clearColor].CGColor;
        _eyeLayer2.lineWidth = 5.f;
        _eyeLayer2.path = path.CGPath;
    }
    return _eyeLayer2;
}

- (CAShapeLayer *)eyeLayer3
{
    if (!_eyeLayer3)
    {
        _eyeLayer3 = [CAShapeLayer layer];
        CGPoint point = CGPointMake(CGRectGetWidth(self.headView.frame) / 2, CGRectGetHeight(self.headView.frame) / 2);
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:point
                                                            radius:CGRectGetWidth(self.headView.frame) * 0.3
                                                        startAngle:(0/180.0) * M_PI
                                                          endAngle:(360/180.0)*M_PI
                                                         clockwise:YES];
        _eyeLayer3.borderColor = [UIColor blackColor].CGColor;
        _eyeLayer3.strokeColor = [UIColor whiteColor].CGColor;
        _eyeLayer3.fillColor = [UIColor clearColor].CGColor;
        _eyeLayer3.lineWidth = 1.f;
        _eyeLayer3.path = path.CGPath;
        _eyeLayer3.anchorPoint = CGPointMake(0.5, 0.5);//设置锚点
    }
    return _eyeLayer3;
}

- (CAShapeLayer *)eyeLayer4
{
    if (!_eyeLayer4)
    {
        _eyeLayer4 = [CAShapeLayer layer];
        CGPoint point = CGPointMake(CGRectGetWidth(self.headView.frame) / 2, CGRectGetHeight(self.headView.frame) / 2);
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(0, CGRectGetHeight(self.headView.frame) / 2)];
        [path addQuadCurveToPoint:CGPointMake(CGRectGetWidth(self.headView.frame), CGRectGetHeight(self.headView.frame) / 2) controlPoint:CGPointMake(CGRectGetWidth(self.headView.frame) / 2, point.y - point.y - 20)];
        _eyeLayer4.borderColor = [UIColor blackColor].CGColor;
        _eyeLayer4.strokeColor = [UIColor whiteColor].CGColor;
        _eyeLayer4.fillColor = [UIColor clearColor].CGColor;
        _eyeLayer4.lineWidth = 1.f;
        _eyeLayer4.path = path.CGPath;
    }
    return _eyeLayer4;
}

- (CAShapeLayer *)eyeLayer5
{
    if (!_eyeLayer5)
    {
        _eyeLayer5 = [CAShapeLayer layer];
        CGPoint point = CGPointMake(CGRectGetWidth(self.headView.frame) / 2, CGRectGetHeight(self.headView.frame) / 2);
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(0, CGRectGetHeight(self.headView.frame) / 2)];
        [path addQuadCurveToPoint:CGPointMake(CGRectGetWidth(self.headView.frame), CGRectGetHeight(self.headView.frame) / 2) controlPoint:CGPointMake(CGRectGetWidth(self.headView.frame) / 2, point.y + point.y + 20)];
        _eyeLayer5.borderColor = [UIColor blackColor].CGColor;
        _eyeLayer5.strokeColor = [UIColor whiteColor].CGColor;
        _eyeLayer5.fillColor = [UIColor clearColor].CGColor;
        _eyeLayer5.lineWidth = 1.f;
        _eyeLayer5.path = path.CGPath;
    }
    return _eyeLayer5;
}

- (void)setAnimation
{
    self.eyeLayer1.lineWidth = 0.f;
    self.eyeLayer2.lineWidth = 0.f;
    self.eyeLayer3.opacity = 0.f;
    self.eyeLayer4.strokeStart = 0.5f;
    self.eyeLayer4.strokeEnd = 0.5f;
    self.eyeLayer5.strokeStart = 0.5f;
    self.eyeLayer5.strokeEnd = 0.5f;
}

- (void)animationWithY:(CGFloat)y
{
    CGFloat flag = self.view.bounds.origin.y * 2.f - 20.f;
    if (y < flag)
    {
        if (self.eyeLayer1.lineWidth < 5.f)
        {
            self.eyeLayer1.lineWidth += 1.f;
            self.eyeLayer2.lineWidth += 1.f;
        }
    }
    
    if (y < flag - 20)
    {
        if (self.eyeLayer3.opacity <= 1.0f)
        {
            self.eyeLayer3.opacity += 0.1f;
        }
    }
    
    if (y < flag - 40)
    {
        if ((self.eyeLayer4.strokeEnd < 1.0f) && (self.eyeLayer4.strokeStart > 0.f))
        {
            self.eyeLayer4.strokeEnd += 0.1f;
            self.eyeLayer4.strokeStart -= 0.1f;
            self.eyeLayer5.strokeEnd += 0.1f;
            self.eyeLayer5.strokeStart -= 0.1f;
        }
    }
    
    if (y > flag - 40)
    {
        if ((self.eyeLayer4.strokeEnd > 0.5f) && (self.eyeLayer4.strokeStart < 0.5f))
        {
            self.eyeLayer4.strokeEnd -= 0.1f;
            self.eyeLayer4.strokeStart += 0.1f;
            self.eyeLayer5.strokeEnd -= 0.1f;
            self.eyeLayer5.strokeStart += 0.1f;
        }
    }
    
    if (y > flag - 20)
    {
        if (self.eyeLayer3.opacity >= 0.0f)
        {
            self.eyeLayer3.opacity -= 0.1f;
        }
    }
    
    if (y > flag)
    {
        if (self.eyeLayer1.lineWidth > 0.0f)
        {
            self.eyeLayer1.lineWidth -= 1.f;
            self.eyeLayer2.lineWidth -= 1.f;
        }
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"自娱自乐";
    
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    bg.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:bg];
    [bg addSubview:self.headView];
    
    lists = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tool" ofType:@"plist"]];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - 64) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.headView.layer addSublayer:self.eyeLayer1];
    [self.headView.layer addSublayer:self.eyeLayer2];
    [self.headView.layer addSublayer:self.eyeLayer3];
    [self.headView.layer addSublayer:self.eyeLayer4];
    [self.headView.layer addSublayer:self.eyeLayer5];
    
    [self setAnimation];
}

- (UIView *)headView
{
    if (!_headView)
    {
        CGFloat headViewH = 40.0;
        CGFloat headViewW = 60.0;
        _headView = [[UIView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.bounds)-headViewW)/2.0, IS_IPHONE_X ? 30 : 10, headViewW, headViewH)];
    }
    return _headView;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return lists.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifier"];
    }
    cell.textLabel.text = lists[indexPath.row][@"title"];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = [UIColor blackColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *vc = [NSClassFromString(lists[indexPath.row][@"class"]) new];
    vc.title = lists[indexPath.row][@"title"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self animationWithY:scrollView.contentOffset.y];
}
@end
