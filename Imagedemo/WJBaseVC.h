//
//  WJBaseVC.h
//  画图demo
//
//  Created by 文杰 许                       on 2018/3/22.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+ImageTool.h"

@interface WJBaseVC : UIViewController
@property (nonatomic, strong) UIImageView *imageView;
@end
