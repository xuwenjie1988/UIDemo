//
//  UIImage+ImageTool.m
//  画图demo
//
//  Created by 文杰 许                       on 2018/3/22.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "UIImage+ImageTool.h"

@implementation UIImage (ImageTool)
#pragma mark -- 生成图片
+ (UIImage *)createImageColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, size.width, size.height)];
    [color setFill];
    [path fill];
    
//    CGContextRef ref = UIGraphicsGetCurrentContext(); 此方式效果同上
//    CGContextSetFillColorWithColor(ref, color.CGColor);
//    CGContextFillRect(ref, CGRectMake(0, 0, size.width, size.height));
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark -- 绘制图片
+ (UIImage *)drawImage:(UIImage *)image size:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark -- 缩放
+ (UIImage *)scaleImage:(UIImage *)image sclae:(CGFloat)scale
{
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    CGSize size = CGSizeMake(width, height);
    
    UIGraphicsBeginImageContext(size);
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark -- 水印文字
+ (UIImage *)waterAtImage:(UIImage *)image
                     text:(NSString *)text
                    point:(CGPoint)point
               attributes:(NSDictionary *)attributes
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    [text drawAtPoint:point withAttributes:attributes];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark -- 水印图片
+ (UIImage *)waterAtImage:(UIImage *)image waterImgae:(UIImage *)waterImage rect:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    [waterImage drawInRect:rect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark -- view快照（截图）
+ (void)cutView:(UIView *)view success:(void (^)(UIImage *))success
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0);
    CGContextRef ref = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ref];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    success(image);
}

#pragma mark -- 擦除效果
+ (UIImage *)wipeView:(UIView *)view point:(CGPoint)point size:(CGSize)size
{
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ref = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ref];
    
    CGFloat clipX = point.x - size.width / 2;
    CGFloat clipY = point.y - size.height / 2;
    CGRect clipRect = CGRectMake(clipX, clipY, size.width, size.height);
    
    CGContextClearRect(ref, clipRect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark -- 矩形区域裁剪
+ (UIImage *)cutImage:(UIImage *)image imageViewSize:(CGSize)size clipRect:(CGRect)rect
{
    CGFloat scaleW = image.size.width / size.width;
    CGFloat scaleH = image.size.height / size.height;
    
    CGRect scaleRect = CGRectMake(rect.origin.x * scaleW ,
                                  rect.origin.y * scaleH,
                                  rect.size.width * scaleW,
                                  rect.size.height * scaleH);
    
    UIGraphicsBeginImageContext(scaleRect.size);
    [image drawAtPoint:CGPointMake(-scaleRect.origin.x, -scaleRect.origin.y)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark -- 不规则区域裁剪
+ (UIImage *)cutImage:(UIImage *)image imageViewSize:(CGSize)size clipPoints:(NSArray *)points
{
    CGFloat scaleW = image.size.width / size.width;
    CGFloat scaleH = image.size.height / size.height;
    
    NSArray *scalePoints = [UIImage points:points scaleX:scaleW scaleY:scaleH];
    
    NSArray *points_x = [scalePoints sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2)
    {
        CGPoint point1 = [obj1 CGPointValue];
        CGPoint point2 = [obj2 CGPointValue];
        return point1.x > point2.x;
    }];
    
    NSArray *points_y = [scalePoints sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2)
    {
        CGPoint point1 = [obj1 CGPointValue];
        CGPoint point2 = [obj2 CGPointValue];
        return point1.y > point2.y;
    }];
    
    CGRect scaleRect = CGRectMake([points_x.firstObject CGPointValue].x,
                                  [points_y.firstObject CGPointValue].y,
                                  [points_x.lastObject CGPointValue].x - [points_x.firstObject CGPointValue].x,
                                  [points_y.lastObject CGPointValue].y - [points_y.firstObject CGPointValue].y);
    
    UIGraphicsBeginImageContext(scaleRect.size);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    for (int i = 0; i < scalePoints.count; i++)
    {
        CGPoint point = [scalePoints[i] CGPointValue];
        if (i == 0)
        {
            [path moveToPoint:point];
        }
        else
        {
            [path addLineToPoint:point];
        }
    }
    [path closePath];
    [path addClip];
    
    [image drawAtPoint:CGPointMake(-scaleRect.origin.x, -scaleRect.origin.y)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (NSArray *)points:(NSArray *)points scaleX:(CGFloat)scaleX scaleY:(CGFloat)scaleY
{
    NSMutableArray *newPoints = [NSMutableArray arrayWithCapacity:points.count];
    for (int i = 0; i < points.count; i++)
    {
        CGPoint point = [[points objectAtIndex:i] CGPointValue];
        CGPoint newPoint = CGPointMake(point.x * scaleX, point.y * scaleY);
        [newPoints addObject:[NSValue valueWithCGPoint:newPoint]];
    }
    return newPoints;
}

#pragma mark -- 特定形状剪切
+ (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage
{
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef),
                                        NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask(image.CGImage, mask);
    
    return [UIImage imageWithCGImage:masked];
}
@end
