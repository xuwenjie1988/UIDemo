//
//  WJButtonVC.m
//  WJView
//
//  Created by 文杰 许                       on 2018/3/23.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WJButtonVC.h"
#import "WJButton.h"

@interface WJButtonVC ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@end

@implementation WJButtonVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    WJButton *btn = [WJButton buttonWithFrame:CGRectMake(100, 100, 100, 100) type:WJButtonTypeHexagram lineJoinType:kCGLineJoinRound lineCapType:kCGLineCapRound color:[UIColor redColor] lineWidth:5.0 scale:1.0 andOnClickAction:^{
        
    }];
    [self.view addSubview:btn];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];// 定义大小
    layout.itemSize = CGSizeMake(50, 50);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumLineSpacing = 10; // 设置item之间的距离，默认不为0，需要设置
    layout.minimumInteritemSpacing = 10;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(5, 10, self.view.bounds.size.width - 10, self.view.bounds.size.height - 10) collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
}

#pragma mark -- UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 180;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:[NSString stringWithFormat:@"%@%ld", NSStringFromClass([UICollectionViewCell class]), (long)indexPath.row]];
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NSString stringWithFormat:@"%@%ld", NSStringFromClass([UICollectionViewCell class]), (long)indexPath.row] forIndexPath:indexPath];
    
    __block WJButton *btn = [WJButton buttonWithFrame:CGRectMake(10, 10, 30, 30) type:indexPath.row+2 lineJoinType:kCGLineJoinRound lineCapType:kCGLineCapRound color:[UIColor redColor] lineWidth:1.0 scale:1.0 andOnClickAction:^{
        
        if (btn.type == WJButtonTypeUp)
        {
            btn.type = WJButtonTypeDown;
        }
        else if(btn.type == WJButtonTypeDown)
        {
            btn.type = WJButtonTypeUp;
        }
        else
        {
            btn.lineWidth = 2.0;
        }
    }];
    
    [cell.contentView addSubview:btn];
    
    return cell;
    
}

@end
