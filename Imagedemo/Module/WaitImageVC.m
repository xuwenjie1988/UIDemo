//
//  WaitImageVC.m
//  Imagedemo
//
//  Created by 文杰 许                       on 2018/3/29.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WaitImageVC.h"

@interface WaitImageVC ()

@end

@implementation WaitImageVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    //开始绘画
    UIGraphicsBeginImageContext(self.view.bounds.size);
    CGContextRef con = UIGraphicsGetCurrentContext();
    
    //填色
    [[UIColor grayColor] setFill];
    
    //设置中心点
    float cX = CGRectGetMidX(self.view.bounds);
    float cY = CGRectGetMidY(self.view.bounds);
    CGContextTranslateCTM(con, cX, cY);
    
    //不断绘画并设置旋转
    for (int i = 0; i < 12; i++)
    {
        CGContextAddRect(con, CGRectMake(-5, 30, 10, 50));
        CGContextFillPath(con);
        CGContextRotateCTM(con, 30 * M_PI / 180);
    }
    
    //结束绘画
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *imagev = [[UIImageView alloc] initWithImage:image];
    [self.view addSubview:imagev];
}

@end
