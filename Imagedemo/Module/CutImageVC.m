//
//  CutImageVC.m
//  ImageTool
//
//  Created by my on 2016/11/22.
//  Copyright © 2016年 my. All rights reserved.
//

#import "CutImageVC.h"
#import "CutImageView.h"

@interface CutImageVC ()
{
    BOOL rule;//是否是规则剪切
    CutImageView *clipImageV;
}
@end

@implementation CutImageVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imageView.image = nil;
    self.imageView.backgroundColor = [UIColor redColor];
    self.view.backgroundColor = [UIColor blackColor];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"裁剪" style:UIBarButtonItemStylePlain target:self action:@selector(clipImage)];
    
    clipImageV = [[CutImageView alloc] initWithFrame:CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y + self.imageView.frame.size.height + 20, 300, 250) type:0];
    clipImageV.image = [UIImage imageNamed:@"ruhua.jpg"];
    [self.view addSubview:clipImageV];
}


- (void)clipImage
{
    [clipImageV clipImage:^(UIImage *image)
    {
        CGRect frame = self.imageView.frame;
        frame.size.height = image.size.height * frame.size.width/image.size.width;
        self.imageView.frame = frame;
        self.imageView.image = image;
    }];
}

@end
