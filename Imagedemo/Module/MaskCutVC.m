//
//  MaskCutVC.m
//  ImageTool
//
//  Created by my on 2016/11/28.
//  Copyright © 2016年 my. All rights reserved.
//

#import "MaskCutVC.h"

@interface MaskCutVC ()

@end

@implementation MaskCutVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UIImage *img = [UIImage maskImage:[UIImage createImageColor:[UIColor redColor] size:CGSizeMake(300, 300)] withMask:[UIImage imageNamed:@"ruhua.jpg"]];
    
    self.imageView.image = img;
}


@end
