//
//  BezierPathVC.m
//  Imagedemo
//
//  Created by 文杰 许                       on 2018/3/30.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "BezierPathVC.h"

@interface BezierPathVC ()

@end

@implementation BezierPathVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGPoint startPoint = CGPointMake(50, 300);
    CGPoint endPoint = CGPointMake(300, 300);
    CGPoint controlPoint1 = CGPointMake(150, 200);
    CGPoint controlPoint2 = CGPointMake(200, 400);
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(startPoint.x, startPoint.y, 5, 5);
    layer1.backgroundColor = [UIColor blueColor].CGColor;
    
    CALayer *layer2 = [CALayer layer];
    layer2.frame = CGRectMake(endPoint.x, endPoint.y, 5, 5);
    layer2.backgroundColor = [UIColor blueColor].CGColor;
    
    CALayer *layer3 = [CALayer layer];
    layer3.frame = CGRectMake(controlPoint1.x, controlPoint1.y, 5, 5);
    layer3.backgroundColor = [UIColor blueColor].CGColor;
    
    CALayer *layer4 = [CALayer layer];
    layer4.frame = CGRectMake(controlPoint2.x, controlPoint2.y, 5, 5);
    layer4.backgroundColor = [UIColor blueColor].CGColor;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    
    [path moveToPoint:startPoint];
    [path addCurveToPoint:endPoint controlPoint1:controlPoint1 controlPoint2:controlPoint2];
    
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = [UIColor redColor].CGColor;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    [self.view.layer addSublayer:shapeLayer];
    [self.view.layer addSublayer:layer1];
    [self.view.layer addSublayer:layer2];
    [self.view.layer addSublayer:layer3];
    [self.view.layer addSublayer:layer4];
    
//    [self addAnimationOneWithlayer:shapeLayer];
    
//    [self addAnimationTwoWithlayer:shapeLayer];
    
    [self addAnimationThreeWithlayer:shapeLayer];
}

- (void)addAnimationOneWithlayer:(CAShapeLayer *)layer
{
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    ani.fromValue = @0;
    ani.toValue = @1;
    ani.duration = 2;
    [layer addAnimation:ani forKey:@""];
}

- (void)addAnimationTwoWithlayer:(CAShapeLayer *)layer
{
    layer.strokeStart = 0.5;
    layer.strokeEnd = 0.5;
    
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    ani.fromValue = @0.5;
    ani.toValue = @0;
    ani.duration = 2;
    
    CABasicAnimation *ani2 = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    ani2.fromValue = @0.5;
    ani2.toValue = @1;
    ani2.duration = 2.0;
    
    [layer addAnimation:ani forKey:@""];
    [layer addAnimation:ani2 forKey:@""];
}

- (void)addAnimationThreeWithlayer:(CAShapeLayer *)layer
{
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"lineWidth"];
    ani.fromValue = @1;
    ani.toValue = @10;
    ani.duration = 2;
    [layer addAnimation:ani forKey:@""];
    
    CABasicAnimation *ani1 = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    ani1.fromValue = @0;
    ani1.toValue = @1;
    ani1.duration = 2;
    [layer addAnimation:ani1 forKey:@""];
}
@end
