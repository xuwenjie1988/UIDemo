//
//  WJMacros.h
//  Imagedemo
//
//  Created by 文杰 许                       on 2018/4/2.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WJColor.h"
#import "WJAppUtil.h"

#ifndef WJMacros_h
#define WJMacros_h

///屏幕尺寸
#define SCREEN_WIDTH   [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_MIN MIN(SCREEN_WIDTH,SCREEN_HEIGHT)
#define SCREEN_MAX MAX(SCREEN_WIDTH,SCREEN_HEIGHT)
#define SCREEN_SCALE [UIScreen mainScreen].scale
#define STATUS_BAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height
#define SCREEN_MARGIN_BOTTOM (DEVICE_IPHONE_X?34.0:0.0)
#define ORIENTATION_LANDSCAPE ([KATAppUtil currentOrientation]==UIDeviceOrientationLandscapeLeft || [KATAppUtil currentOrientation]==UIDeviceOrientationLandscapeRight)
#define ORIENTATION_PORTRAIT ([KATAppUtil currentOrientation]==UIInterfaceOrientationPortrait || [KATAppUtil currentOrientation]==UIInterfaceOrientationPortraitUpsideDown)

//机型适配
#define FIT(value) ((MIN(SCREEN_HEIGHT, SCREEN_WIDTH))/375.0*(value))

///颜色
#define KCOLOR_CLEAR [UIColor clearColor]
#define KCOLOR_WHITE [WJColor colorWithHSBA:COLOR_WHITE]
#define KCOLOR_LIGHT [WJColor colorWithHSBA:COLOR_LIGHT]
#define KCOLOR_GRAY [WJColor colorWithHSBA:COLOR_GRAY]
#define KCOLOR_DARK [WJColor colorWithHSBA:COLOR_DARK]
#define KCOLOR_NIGHT [WJColor colorWithHSBA:COLOR_NIGHT]
#define KCOLOR_BLACK [WJColor colorWithHSBA:COLOR_BLACK]

#define KCOLOR_RED [WJColor colorWithHSBA:COLOR_RED]
#define KCOLOR_FIRE [WJColor colorWithHSBA:COLOR_FIRE]
#define KCOLOR_ORANGE [WJColor colorWithHSBA:COLOR_ORANGE]
#define KCOLOR_BRONZE [WJColor colorWithHSBA:COLOR_BRONZE]
#define KCOLOR_GOLD [WJColor colorWithHSBA:COLOR_GOLD]
#define KCOLOR_YELLOW [WJColor colorWithHSBA:COLOR_YELLOW]
#define KCOLOR_FOREST [WJColor colorWithHSBA:COLOR_FOREST]
#define KCOLOR_GREEN [WJColor colorWithHSBA:COLOR_GREEN]
#define KCOLOR_SPRING [WJColor colorWithHSBA:COLOR_SPRING]
#define KCOLOR_CYAN [WJColor colorWithHSBA:COLOR_CYAN]
#define KCOLOR_SKY [WJColor colorWithHSBA:COLOR_SKY]
#define KCOLOR_LAKE [WJColor colorWithHSBA:COLOR_LAKE]
#define KCOLOR_BLUE [WJColor colorWithHSBA:COLOR_BLUE]
#define KCOLOR_ROYAL [WJColor colorWithHSBA:COLOR_ROYAL]
#define KCOLOR_NAVY [WJColor colorWithHSBA:COLOR_NAVY]
#define KCOLOR_SEA [WJColor colorWithHSBA:COLOR_SEA]
#define KCOLOR_VIOLET [WJColor colorWithHSBA:COLOR_VIOLET]
#define KCOLOR_PURPLE [WJColor colorWithHSBA:COLOR_PURPLE]
#define KCOLOR_PLUM [WJColor colorWithHSBA:COLOR_PLUM]
#define KCOLOR_ROSE [WJColor colorWithHSBA:COLOR_ROSE]
#define KCOLOR_BLOOD [WJColor colorWithHSBA:COLOR_BLOOD]
#define KCOLOR_CRIMSON [WJColor colorWithHSBA:COLOR_CRIMSON]
#define KCOLOR_PINK [WJColor colorWithHSBA:COLOR_PINK]

#define KCOLOR_LINE [WJColor colorWithHSBA:COLOR_LINE]
#define KCOLOR_PAPER [WJColor colorWithHSBA:COLOR_PAPER]
#define KCOLOR_BACKGROUND [WJColor colorWithHSBA:COLOR_BACKGROUND]
#define KCOLOR_SHADOW_LIGHT [WJColor colorWithHSBA:COLOR_SHADOW_LIGHT]
#define KCOLOR_SHADOW [WJColor colorWithHSBA:COLOR_SHADOW]
#define KCOLOR_SHADOW_DARK [WJColor colorWithHSBA:COLOR_SHADOW_DARK]

#define KCOLOR_RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a/255.0]
#define KCOLOR_HSBA(h,s,b,a) [UIColor colorWithHue:h/255.0 saturation:s/255.0 brightness:b/255.0 alpha:a/255.0]
#define RGBA(rgba) [WJColor colorWithRGBA:rgba]
#define RGB(rgb) [WJColor colorWithRGB:rgb]
#define HSBA(hsba) [WJColor colorWithHSBA:hsba]
#define HSB(hsb) [WJColor colorWithHSBA:hsb*100+99]
#define COLOR(color) [WJColor colorWithHSBA:color]

///系统
#define OS_VERSION [KATAppUtil OSVersion]
#define APP_VERSION [KATAppUtil appVersion]
#define DOCUMENTS_PATH [KATAppUtil docPath]
#define APP_PATH [KATAppUtil appPath]
#define TEMP_PATH [KATAppUtil tempPath]
#define LANGUAGE [KATAppUtil language]
#define LANGUAGE_CH (([KATAppUtil language]==SYSTEM_LANGUAGE_CH)?YES:NO)
#define LANGUAGE_CHT (([KATAppUtil language]==SYSTEM_LANGUAGE_CHT)?YES:NO)
#define LANGUAGE_EN (([KATAppUtil language]==SYSTEM_LANGUAGE_EN)?YES:NO)
#define LANGUAGE_JA (([KATAppUtil language]==SYSTEM_LANGUAGE_JA)?YES:NO)
#define LANGUAGE_OTHER (([KATAppUtil language]==SYSTEM_LANGUAGE_OTHER)?YES:NO)

///机型
#define IS_IPHONE_4 ((MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)>=479 && MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)<=481)?YES:NO)
#define IS_IPHONE_5 ((MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)>=567 && MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)<=569)?YES:NO)
#define IS_IPHONE_6 ((MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)>=666 && MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)<=668)?YES:NO)
#define IS_IPHONE_6_PLUS ((MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)>=735 && MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)<=737)?YES:NO)
#define IS_IPHONE_X ((MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)>=811 && MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)<=813)?YES:NO)
#define IS_IPAD ((MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)>=1023 && MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)<=1025)?YES:NO)
#define IS_IPAD_PRO ((MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)>=1365 && MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)<=1367)?YES:NO)

///GCD
#define GCD_ASYNC_MAIN(block) dispatch_async(dispatch_get_main_queue(),^block)
#define GCD_ASYNC_GLOBAL(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^block)
#define GCD_SYNC_MAIN(block) dispatch_sync(dispatch_get_main_queue(),^block)
#define GCD_SYNC_GLOBAL(block) dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^block)
#define GCD_AFTER(seconds,queue,block) dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), queue, ^block)
#define GCD_MAIN_AFTER(seconds,block) dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^block)

///常用结构体
#define RECT(x,y,w,h) CGRectMake(x, y, w, h)
#define SIZE(w,h) CGSizeMake(w, h)
#define POINT(x,y) CGPointMake(x, y)
#define RANGE(o,l) NSMakeRange(o, l)
#define BOUNDS(w,h) CGRectMake(0, 0, w, h)

// 是否是空数组
#define AVAILABLE_ARR(_ARRAY___) (_ARRAY___ && [_ARRAY___ isKindOfClass:[NSArray class]] && [_ARRAY___ count])
// 是否是空字符串
#define AVAILABLE_STR(_STRING___) (_STRING___ && [_STRING___ isKindOfClass:[NSString class]] && [_STRING___ length])
// 是否是空字典
#define AVAILABLE_DICT(_DICTIONARY___) (_DICTIONARY___ && [_DICTIONARY___ isKindOfClass:[NSDictionary class]] && [_DICTIONARY___ count])
// 是否是空对象
#define AVAILABLE_OBJ(____object____) ((____object____ && [____object____ isKindOfClass: [NSNull class]]) || (____object____ == nil))
/// 字符串拼接
#define  AMEND_STR(obj)  ((AVAILABLE_OBJ(obj))?(@""):([NSString stringWithFormat:@"%@",obj]))

// 圆角
#define WJViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]
#endif /* WJMacros_h */
