//
//  WJButton.h
//  WJView
//
//  Created by 文杰 许                       on 2018/3/23.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import <UIKit/UIKit.h>

//按钮类型枚举
typedef NS_ENUM(NSUInteger, WJButtonType)
{
    WJButtonTypeNone=0,
    WJButtonTypeEmpty=1,
    
    //方向
    WJButtonTypeUp,
    WJButtonTypeDown,
    WJButtonTypeLeft,
    WJButtonTypeRight,
    WJButtonTypeDoubleUp,
    WJButtonTypeDoubleDown,
    WJButtonTypeDoubleLef,
    WJButtonTypeDoubleRight,
    WJButtonTypeUpSide,
    WJButtonTypeDownSide,
    WJButtonTypeLeftSide,
    WJButtonTypeRightSide,
    WJButtonTypeArrowUp,
    WJButtonTypeArrowDown,
    WJButtonTypeArrowLeft,
    WJButtonTypeArrowRight,
    WJButtonTypeRoundUp,
    WJButtonTypeRoundDown,
    WJButtonTypeRoundLeft,
    WJButtonTypeRoundRight,
    
    //形状
    WJButtonTypeRectangle,
    WJButtonTypeTriangle,
    WJButtonTypeRound,
    WJButtonTypeRoundedRectangle,
    WJButtonTypeStar,
    WJButtonTypeHeart,
    WJButtonTypeSharpRectangle,
    WJButtonTypeSharpTriangle,
    WJButtonTypeFilledRectangle,
    WJButtonTypeFilledTriangle,
    WJButtonTypeFilledRound,
    WJButtonTypeFilledRoundedRectangle,
    WJButtonTypeFilledStar,
    WJButtonTypeFilledHeart,
    WJButtonTypeFilledSharpRectangle,
    WJButtonTypeFilledSharpTriangle,
    WJButtonTypePentagram,
    WJButtonTypeHexagram,
    WJButtonTypePoint,
    
    //播放
    WJButtonTypePlayerPlay,
    WJButtonTypePlayerPause,
    WJButtonTypePlayerStop,
    WJButtonTypePlayerFilledPlay,
    WJButtonTypePlayerFilledPause,
    WJButtonTypePlayerFilledStop,
    WJButtonTypePlayerLinearPlay,
    WJButtonTypePlayerLinearPause,
    WJButtonTypePlayerSoundOn,
    WJButtonTypePlayerSoundOff,
    
    //编辑
    WJButtonTypeOK,
    WJButtonTypeClose,
    WJButtonTypeEdit,
    WJButtonTypeRemove,
    WJButtonTypeAdd,
    WJButtonTypeReduce,
    WJButtonTypeCheck,
    WJButtonTypeBackspace,
    WJButtonTypeSetting,
    WJButtonTypeEnter,
    WJButtonTypeLeave,
    WJButtonTypeSort,
    WJButtonTypePick,
    
    //图标
    WJButtonTypeSearch,
    WJButtonTypeZoomIn,
    WJButtonTypeZoomOut,
    WJButtonTypeUser,
    WJButtonTypePlus,
    WJButtonTypeMinus,
    WJButtonTypeMultiply,
    WJButtonTypeDivision,
    WJButtonTypePercent,
    WJButtonTypeReload,
    WJButtonTypeUpdate,
    WJButtonTypeDownload,
    WJButtonTypeUpload,
    WJButtonTypeRefresh,
    WJButtonTypeLocation,
    WJButtonTypeNearby,
    WJButtonTypeCoordinate,
    WJButtonTypeSend,
    WJButtonTypeMail,
    WJButtonTypeComment,
    WJButtonTypeChat,
    WJButtonTypeFocus,
    WJButtonTypeRoundedFocus,
    WJButtonTypeScan,
    WJButtonTypeRoundedScan,
    WJButtonTypeCamera,
    WJButtonTypeCalendar,
    WJButtonTypeBluetooth,
    WJButtonTypeBrightness,
    WJButtonTypeNight,
    WJButtonTypeMicphone,
    WJButtonTypeFlying,
    WJButtonTypeHome,
    WJButtonTypeMore,
    WJButtonTypeList,
    WJButtonTypeClock,
    WJButtonTypeDocument,
    WJButtonTypeBalance,
    WJButtonTypeBooking,
    WJButtonTypeCure,
    WJButtonTypeAid,
    WJButtonTypeMicroscope,
    WJButtonTypeWifi,
    WJButtonTypeBroadcast,
    WJButtonTypeBroadcast2,
    WJButtonTypeSubscription,
    WJButtonTypeNorth,
    WJButtonTypeArticle,
    WJButtonTypeCloud,
    WJButtonTypeCloudDownload,
    WJButtonTypeCloudUpload,
    WJButtonTypeLock,
    WJButtonTypeImage,
    WJButtonTypeBrokenImage,
    WJButtonTypeFemale,
    WJButtonTypeMale,
    WJButtonTypeSun,
    WJButtonTypeMoon,
    WJButtonTypeLightning,
    WJButtonTypeInfomation,
    WJButtonTypeForbid,
    WJButtonTypeBan,
    WJButtonTypeQuestion,
    WJButtonTypeCaution,
    WJButtonTypeTarget,
    WJButtonTypeCall,
    WJButtonTypePhone,
    WJButtonTypeAries,
    WJButtonTypeTaurus,
    WJButtonTypeGemini,
    WJButtonTypeCancer,
    WJButtonTypeLeo,
    WJButtonTypeVirgo,
    WJButtonTypeLibra,
    WJButtonTypeScorpio,
    WJButtonTypeSagittarius,
    WJButtonTypeCapricorn,
    WJButtonTypeAquarius,
    WJButtonTypePisces,
    
    //图表
    WJButtonTypeLineChart,
    WJButtonTypeLineChart2,
    WJButtonTypeLineChart3,
    WJButtonTypeAreaChart,
    WJButtonTypeAreaChart2,
    WJButtonTypeBarChart,
    WJButtonTypeBarChart2,
    WJButtonTypeStackChart,
    WJButtonTypePieChart,
    
    //键盘
    WJButtonTypeKeyboard0,
    WJButtonTypeKeyboard1,
    WJButtonTypeKeyboard2,
    WJButtonTypeKeyboard3,
    WJButtonTypeKeyboard4,
    WJButtonTypeKeyboard5,
    WJButtonTypeKeyboard6,
    WJButtonTypeKeyboard7,
    WJButtonTypeKeyboard8,
    WJButtonTypeKeyboard9,
    WJButtonTypeKeyboardPoint,
    WJButtonTypeKeyboardEqual,
    WJButtonTypeKeyboardPlus,
    WJButtonTypeKeyboardMinus,
    WJButtonTypeKeyboardMultipy,
    WJButtonTypeKeyboardDivision,
    WJButtonTypeKeyboardPower,
    WJButtonTypeKeyboardPercent,
    WJButtonTypeKeyboardLeftBrackets,
    WJButtonTypeKeyboardRightBrackets,
    WJButtonTypeKeyboardBlank,
    WJButtonTypeKeyboardReturn,
    WJButtonTypeKeyboardSlash,
    WJButtonTypeKeyboardShape,
};

@class WJButton;


//按钮事件协议
@protocol WJButtonDelegate <NSObject>

@optional

/// 按钮点击事件
- (void)clickButton:(WJButton *)button;

@end


@interface WJButton : UIView

#pragma -mark 属性

///颜色
@property(nonatomic, strong) UIColor *color;

///类型
@property(nonatomic, assign) WJButtonType type;

///接头处的风格: kCGLineJoinMiter:斜接;  kCGLineJoinRound:圆角;  kCGLineJoinBevel:斜角
@property(nonatomic, assign) CGLineJoin lineJoin;

///端点处的风格: kCGLineCapButt, kCGLineCapSquare  这两个风格都是直角风格; kCGLineCapRound,圆角风格
@property(nonatomic, assign) CGLineCap lineCap;

///线宽
@property(nonatomic, assign) float lineWidth;

///缩放比例
@property(nonatomic, assign) float scale;

///事件代理
@property(nonatomic, weak) id<WJButtonDelegate> delegate;

///点击事件代码块
@property(nonatomic, copy) void (^onClickAction)(void);

#pragma -mark 携带值

///int值
@property(nonatomic, assign) long long index;

///double值
@property(nonatomic, assign) double value;

///字符串值
@property(nonatomic, copy) NSString *message;

///id值
@property(nonatomic, strong) id object;

#pragma -mark 类方法


/**
 获取实例(简化方法)
 
 @param frame 左上角位置和宽高
 @return 按钮实例
 */
+ (instancetype)buttonWithFrame:(CGRect)frame;


/**
 获取实例(全参数方法)
 
 @param frame 左上角位置和宽高
 @param type 类型
 @param lineJoin 交叉点的类型
 @param lineCap 终点类型
 @param color 颜色
 @param lineWidth 线宽
 @param scale 缩放比例(1.0为正常的显示比例，小于1.0则显示更小的按钮形状，但不影响点击范围，点击范围由frame决定)
 @param action 点击事件代码块(可以为nil)
 @return 按钮实例
 */
+ (instancetype)buttonWithFrame:(CGRect)frame type:(WJButtonType)type
                   lineJoinType:(CGLineJoin)lineJoin
                    lineCapType:(CGLineCap)lineCap
                          color:(UIColor *)color
                      lineWidth:(float)lineWidth
                          scale:(float)scale
               andOnClickAction:(void (^)(void))action;



#pragma -mark 对象方法


/**
 按钮转化为图片
 @return 内容为按钮形状的图片
 */
- (UIImage *)button2Image;

@end
