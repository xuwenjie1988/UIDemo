//
//  WJButton.m
//  WJView
//
//  Created by 文杰 许                       on 2018/3/23.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "WJButton.h"

@interface WJButton()
///点击手势
@property(nonatomic, strong) UITapGestureRecognizer *tap;

///长按手势
@property(nonatomic, strong) UILongPressGestureRecognizer *press;
@end

@implementation WJButton
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

+ (instancetype)buttonWithFrame:(CGRect)frame
{
    return [self buttonWithFrame:frame type:WJButtonTypeEmpty lineJoinType:kCGLineJoinRound lineCapType:kCGLineCapRound color:[UIColor blueColor] lineWidth:1.0 scale:1.0 andOnClickAction:nil];
}

+ (instancetype)buttonWithFrame:(CGRect)frame
                           type:(WJButtonType)type
                   lineJoinType:(CGLineJoin)lineJoin
                    lineCapType:(CGLineCap)lineCap
                          color:(UIColor *)color
                      lineWidth:(float)lineWidth
                          scale:(float)scale
               andOnClickAction:(void (^)(void))action
{
    if (type == WJButtonTypeNone)
    {
        return nil;
    }
    
    WJButton *button = [[self alloc] init];
    
    button.frame = frame;
    button.backgroundColor = [UIColor clearColor];
    button.color = color;
    button.type = type;
    button.lineJoin = lineJoin;
    button.lineCap = lineCap;
    button.lineWidth = lineWidth;
    button.scale = scale;
    
    button.index = 0;
    button.value = 0.0;
    button.message = nil;
    button.object = nil;
    
    button.tap = [[UITapGestureRecognizer alloc] initWithTarget:button action:@selector(buttonTaped)];
    button.tap.numberOfTapsRequired = 1;
    
    button.press = [[UILongPressGestureRecognizer alloc] initWithTarget:button action:@selector(buttonPressed:)];
    button.press.minimumPressDuration = 0.3;
    
    button.delegate = nil;
    button.onClickAction=action;
    
    return button;
}

// 设置frame
- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self setNeedsDisplay];
}

// 设置颜色
- (void)setColor:(UIColor *)color
{
    _color = color;
    
    [self setNeedsDisplay];
}

// 设置类型
- (void)setType:(WJButtonType)type
{
    _type = type;
    
    [self setNeedsDisplay];
}

// 设置接头处的风格
- (void)setLineJoin:(CGLineJoin)lineJoin
{
    _lineJoin = lineJoin;
    
    [self setNeedsDisplay];
}

// 设置端点处的风格
- (void)setLineCap:(CGLineCap)lineCap
{
    _lineCap = lineCap;
    
    [self setNeedsDisplay];
}

// 设置线宽
- (void)setLineWidth:(float)lineWidth
{
    _lineWidth = lineWidth;
    
    [self setNeedsDisplay];
}

// 设置缩放比例
- (void)setScale:(float)scale
{
    _scale = scale;
    
    [self setNeedsDisplay];
}

// 点击事件
- (void)setOnClickAction:(void (^)(void))onClickAction
{
    if (onClickAction)
    {
        _onClickAction = onClickAction;
        
        //打开用户交互
        self.userInteractionEnabled = YES;
        
        //添加手势
        [self addGestureRecognizer:_tap];
        [self addGestureRecognizer:_press];
    }
    else
    {
        _onClickAction = nil;
        
        if (!_delegate)
        {
            //移除手势
            [self removeGestureRecognizer:_tap];
            [self removeGestureRecognizer:_press];
        }
    }
}

- (void)setDelegate:(id<WJButtonDelegate>)delegate
{
    _delegate = delegate;
    
    if (_delegate)
    {
        //打开用户交互
        self.userInteractionEnabled = YES;
        
        [self addGestureRecognizer:_tap];
        [self addGestureRecognizer:_press];
    }
    else
    {
        if (!_onClickAction)
        {
            [self removeGestureRecognizer:_tap];
            [self removeGestureRecognizer:_press];
        }
    }
}

// 转化为图片
- (UIImage *)button2Image
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

// 点击事件
- (void)buttonTaped
{
    //回调
    if(_onClickAction)
    {
        _onClickAction();
    }
    
    if(_delegate && [_delegate respondsToSelector:@selector(clickButton:)])
    {
        [_delegate clickButton:self];
    }
}

// 长按事件
- (void)buttonPressed:(UILongPressGestureRecognizer *)recognizer
{
    if(recognizer.state==UIGestureRecognizerStateBegan)//开始，变色
    {
        self.alpha=0.4;
    }
    else if(recognizer.state==UIGestureRecognizerStateEnded)//完成
    {
        self.alpha=1.0;
        
        CGPoint point=[recognizer locationInView:self];
        
        if(point.x<0.0 || point.x>self.bounds.size.width || point.y<0.0 || point.y>self.bounds.size.height)//超出边界
        {
            
        }
        else
        {
            //回调
            if(_onClickAction)
            {
                _onClickAction();
            }
            
            if(_delegate && [_delegate respondsToSelector:@selector(clickButton:)])
            {
                [_delegate clickButton:self];
            }
        }
    }
    else if(recognizer.state==UIGestureRecognizerStateChanged)//改变
    {
        CGPoint point=[recognizer locationInView:self];
        
        if(point.x<0.0 || point.x>self.bounds.size.width || point.y<0.0 || point.y>self.bounds.size.height)//超出边界
        {
            self.alpha=1.0;
        }
        else
        {
            self.alpha=0.4;
        }
    }
    else
    {
        self.alpha=1.0;
    }
}

//显示
- (void)drawRect:(CGRect)rect
{
    CGContextRef context=UIGraphicsGetCurrentContext();//获得当前view的图形上下文(context)
    
    CGContextSetStrokeColorWithColor(context, _color.CGColor);//设置线段颜色
    CGContextSetFillColorWithColor(context, _color.CGColor);//设置填充颜色

    CGContextSetLineWidth(context, _lineWidth);
    CGContextSetLineJoin(context, _lineJoin);
    CGContextSetLineCap(context, _lineCap);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [_color set];
    [path setLineWidth:_lineWidth];
    [path setLineJoinStyle:_lineJoin];
    [path setLineCapStyle:_lineCap];
    
    //缩放公式w*(0.x+(0.5-0.x)*(1.0-_scale))
    switch(_type)
    {
        case WJButtonTypeNone:
        case WJButtonTypeEmpty:

            break;
            
#pragma -mark 方向
            
        case WJButtonTypeUp://上
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeDown://下
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeLeft://左
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRight://右
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeDoubleUp://双上
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeDoubleDown://双下
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeDoubleLef://双左
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeDoubleRight://双右
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeUpSide://上边
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            
            [path stroke];
            
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeDownSide://下边
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            
            [path stroke];
            
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeLeftSide://左边
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.7+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
            
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRightSide://右边
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
            
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeArrowUp://箭头（向上）
        {
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.19+(0.5-0.19)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.81+(0.5-0.81)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeArrowDown://箭头（向下）
        {
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.19+(0.5-0.19)*(1.0-_scale)), self.bounds.size.height*(0.64+(0.5-0.64)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.81+(0.5-0.81)*(1.0-_scale)), self.bounds.size.height*(0.64+(0.5-0.64)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeArrowLeft://箭头（向左）
        {
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.36+(0.5-0.36)*(1.0-_scale)), self.bounds.size.height*(0.19+(0.5-0.19)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.36+(0.5-0.36)*(1.0-_scale)), self.bounds.size.height*(0.81+(0.5-0.81)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
           
        case WJButtonTypeArrowRight://箭头（向右）
        {
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.64+(0.5-0.64)*(1.0-_scale)), self.bounds.size.height*(0.19+(0.5-0.19)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.64+(0.5-0.64)*(1.0-_scale)), self.bounds.size.height*(0.81+(0.5-0.81)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRoundUp://上（带圆环）
        {
            //圆  radius:半径。startAngle:开始角度。endAngle:结束角度 clockwise:是否顺时针
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRoundDown://下（带圆环）
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRoundLeft://左（带圆环）
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRoundRight://右（带圆环）
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            [path stroke];
            
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
            
#pragma -mark 形状
            
            
        case WJButtonTypeRectangle://矩形
            
            //小圆角矩形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeTriangle://三角形
        {
            //小圆角三角形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale))+self.bounds.size.height*(0.087*_scale))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale))+self.bounds.size.height*(0.087*_scale)) controlPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale))-self.bounds.size.height*(0.087*_scale))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.087*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.087*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale))-self.bounds.size.height*(0.087*_scale)) controlPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)))];

            [path closePath];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRound://圆
            
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];

            [path stroke];
            
            break;
            
            
        case WJButtonTypeRoundedRectangle://圆角矩形
            
            //圆角矩形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.2*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.2*_scale), self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.width*(0.2*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.2*_scale), self.bounds.size.height*(0.2*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeStar://五角星
        {
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.048+(0.5-0.048)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.393+(0.5-0.393)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.069+(0.5-0.069)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.607+(0.5-0.607)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.952+(0.5-0.952)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.672+(0.5-0.672)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.783+(0.5-0.783)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.727+(0.5-0.727)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.328+(0.5-0.328)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.048+(0.5-0.048)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            
            [path stroke];
            
        }
            
            break;
            
        case WJButtonTypeHeart://心形
        {
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)))];
            [path addCurveToPoint:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint1:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale))) controlPoint2:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint1:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale))) controlPoint2:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale)))];
            
            [path closePath];
            [path stroke];
            
        }
            
            break;
            
            
        case WJButtonTypeSharpRectangle://尖角矩形
        {
            //矩形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeSharpTriangle://尖角三角形
        {
            //三角形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeFilledRectangle://矩形填充
            
            //小圆角矩形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            break;
            
            
        case WJButtonTypeFilledTriangle://三角形填充
            
            //小圆角三角形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale))+self.bounds.size.height*(0.087*_scale));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale))+self.bounds.size.height*(0.087*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale))-self.bounds.size.height*(0.087*_scale));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.087*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.087*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale))-self.bounds.size.height*(0.087*_scale));
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            break;
            
            
        case WJButtonTypeFilledRound://圆填充
            
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            [path fill];
            
            break;
            
            
        case WJButtonTypeFilledRoundedRectangle://圆角矩形填充
            
            //圆角矩形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.2*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.2*_scale), self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.width*(0.2*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.2*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.2*_scale), self.bounds.size.height*(0.2*_scale));
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            break;
            
            
        case WJButtonTypeFilledStar://五角星填充
            
        {
            //填充五角星
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.048+(0.5-0.048)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.393+(0.5-0.393)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.069+(0.5-0.069)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.607+(0.5-0.607)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.952+(0.5-0.952)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.672+(0.5-0.672)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.783+(0.5-0.783)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.727+(0.5-0.727)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.328+(0.5-0.328)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.048+(0.5-0.048)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            
            [path fill];
            
            //描边五角星
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.048+(0.5-0.048)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.393+(0.5-0.393)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.069+(0.5-0.069)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.607+(0.5-0.607)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.952+(0.5-0.952)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.672+(0.5-0.672)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.783+(0.5-0.783)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.727+(0.5-0.727)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.328+(0.5-0.328)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.048+(0.5-0.048)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeFilledHeart://心形填充，半透明状态时会有三角形印记
        {
            //填充
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)))];
            [path addCurveToPoint:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint1:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale))) controlPoint2:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint1:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale))) controlPoint2:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale)))];
            
            [path fill];
            
            //描边
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)))];
            [path addCurveToPoint:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint1:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale))) controlPoint2:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale))) controlPoint1:CGPointMake(self.bounds.size.width*(0.94+(0.5-0.94)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale))) controlPoint2:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.02+(0.5-0.02)*(1.0-_scale)))];
            
            [path stroke];
            
        }
            
            break;
            
        case WJButtonTypeFilledSharpRectangle://尖角矩形填充
            
        {
            //矩形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path fill];
        }
            
            break;
            
            
        case WJButtonTypeFilledSharpTriangle://尖角三角形填充
            
        {
            //三角形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale)))];
            
            [path fill];
        }
            
            break;
            
            
        case WJButtonTypePentagram://五芒星
        {
            //五芒星
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.048+(0.5-0.048)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.952+(0.5-0.952)*(1.0-_scale)), self.bounds.size.height*(0.397+(0.5-0.397)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.069+(0.5-0.069)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.783+(0.5-0.783)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)))];
            
            [path closePath];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeHexagram://六芒星
        {
            //三角形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.height*(0.725+(0.5-0.725)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.725+(0.5-0.725)*(1.0-_scale)))];
            
            [path closePath];
            [path stroke];
            
            //三角形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale)))];
            
            [path closePath];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePoint://点
            
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            [path fill];
            
            break;
            
            
            
#pragma -mark 播放
            
            
        case WJButtonTypePlayerPlay://播放
       
        {
            //三角形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePlayerPause://暂停
        {
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path appendPath:path2];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePlayerStop://停止
            
        {
            //矩形
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePlayerFilledPlay://播放
            
        {
            //三角形填充
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale))-_lineWidth*0.2, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path fill];
            
            //三角形描边
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePlayerFilledPause://暂停
            
        {
            //矩形填充
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale))-_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale))-_lineWidth*0.2, self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            
            //矩形描边
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            
            //矩形填充
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2, self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2)];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2)];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            
            //矩形描边
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path appendPath:path2];
            
            [path fill];
            [path stroke];
            
        }
    
            break;
            
        case WJButtonTypePlayerFilledStop://停止
        
        {
            //正方形填充
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2, self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-_lineWidth*0.2)];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2, self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*0.2)];
            
            [path fill];
            
            //正方形描边
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePlayerLinearPlay://播放(线型)
            
        {
            //尖头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePlayerLinearPause://暂停(线型)
            
        {
            //线段1
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            
            //线段2
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePlayerSoundOn://开启声音
        {
            //喇叭
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            //声波
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))) radius:self.bounds.size.width*(0.45*_scale) startAngle:M_PI*1.75 endAngle:M_PI*0.25 clockwise:YES];
            
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))) radius:self.bounds.size.width*(0.25*_scale) startAngle:M_PI*1.75 endAngle:M_PI*0.25 clockwise:YES];
            //喇叭内弧线
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))) radius:self.bounds.size.width*(0.15*_scale) startAngle:M_PI*0.7 endAngle:M_PI*1.3 clockwise:YES];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path appendPath:path4];
            [path stroke];
        }
            //小圆角喇叭
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))+self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
//            CGContextClosePath(context);
//
//            CGContextStrokePath(context);
            
            //声波
//            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.45*_scale), M_PI*1.75, M_PI*0.25,0);
//
//            CGContextStrokePath(context);
//
//            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.25*_scale), M_PI*1.75, M_PI*0.25,0);
//
//            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypePlayerSoundOff://关闭声音
            
        {
            //直角喇叭
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            //叉叉
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path stroke];
        }
            //喇叭
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))+self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
//            CGContextClosePath(context);
//
//            CGContextStrokePath(context);
//
//            //叉
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
            
            break;
            
            
            
#pragma -mark 编辑
            
            
        case WJButtonTypeOK://OK
        {
            //勾
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeClose://关闭
        {
            //叉叉
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeEdit://编辑
        {
            //本子
            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //笔身
            CGContextMoveToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //笔尖
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //笔帽
            CGContextMoveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeRemove://删除

        {
            //轮廓
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //盖子
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //条纹
            CGContextMoveToPoint(context, self.bounds.size.width*(0.37+(0.5-0.37)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.37+(0.5-0.37)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.63+(0.5-0.63)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.63+(0.5-0.63)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
        }
            
            break;
            
        case WJButtonTypeAdd://添加
            
        {
            //十字
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)))];
            
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeReduce://减
            
        {
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeCheck://勾选
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeBackspace://退格
            
        {
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.2*_scale), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //叉
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeSetting://设置
            
        {
            float startAngle=0;//开始角度
            float radius=self.bounds.size.height*(0.45*_scale);//半径
            
            CGContextMoveToPoint(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius*(cosf(startAngle-M_PI/16.0)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius*(sinf(startAngle-M_PI/16.0)));
            
            for (int i=0; i<8; i++)
            {
                CGContextAddCurveToPoint(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius/1.62*(cosf(startAngle+M_PI/4.0*i-M_PI/16.0)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius/1.62*(sinf(startAngle+M_PI/4.0*i-M_PI/16.0)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius/1.62*(cosf(startAngle+M_PI/4.0*i+M_PI/16.0)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius/1.62*(sinf(startAngle+M_PI/4.0*i+M_PI/16.0)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius*(cosf(startAngle+M_PI/4.0*i+M_PI/16.0)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+radius*(sinf(startAngle+M_PI/4.0*i+M_PI/16.0)));
                
                CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), radius, startAngle+M_PI/4.0*i+M_PI/16.0, startAngle+M_PI/4.0*i+startAngle+M_PI/8.0+M_PI/16.0, 0);
            }
            
            CGContextStrokePath(context);
            
            //内圆
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), radius*0.38, M_PI*0, M_PI*2.0, 0);
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeEnter://进入
            
        {
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeLeave://离开
            
        {
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddArcToPoint(context,self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeSort://排序
            
        {
            //上箭头
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            
            //下箭头
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePick://图片拾取
            
        {
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //十字
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
            
#pragma -mark 图标
            
            
        case WJButtonTypeSearch://搜索  两种方法效果一样
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))) radius:self.bounds.size.height*(0.3*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            [path stroke];
            
            //手柄
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 setLineWidth:2*_lineWidth];
            [path2 setLineCapStyle:_lineCap];
            [path2 setLineJoinStyle:_lineJoin];
            
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.button2Image.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            
            [path2 stroke];
        }
            //圆
//            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.6*_scale), self.bounds.size.height*(0.6*_scale)));
//
//            CGContextStrokePath(context);
//
//            //手柄
//            CGContextSetLineWidth(context, 2*_lineWidth);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale))+_lineWidth);
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeZoomIn://放大
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))) radius:self.bounds.size.height*(0.3*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            //加号
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path stroke];
            
            //手柄
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 setLineWidth:2*_lineWidth];
            [path4 setLineCapStyle:_lineCap];
            [path4 setLineJoinStyle:_lineJoin];
            
            [path4 moveToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.button2Image.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            
            [path4 stroke];
        }
            //圆
//            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.6*_scale), self.bounds.size.height*(0.6*_scale)));
//            CGContextStrokePath(context);
//
//            //加号
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
//
//            //手柄
//            CGContextSetLineWidth(context, 2*_lineWidth);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale))+_lineWidth);
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
//            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeZoomOut://缩小
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))) radius:self.bounds.size.height*(0.3*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            //减号
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
            
            //手柄
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 setLineWidth:2*_lineWidth];
            [path4 setLineCapStyle:_lineCap];
            [path4 setLineJoinStyle:_lineJoin];
            
            [path4 moveToPoint:CGPointMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.button2Image.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            
            [path4 stroke];
        }
            //圆
//            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.6*_scale), self.bounds.size.height*(0.6*_scale)));
//            CGContextStrokePath(context);
//
//            //减号
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
//            CGContextStrokePath(context);
//
//            //手柄
//            CGContextSetLineWidth(context, 2*_lineWidth);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale))+_lineWidth);
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeUser://用户
            
        {
            //头
            [path addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale))) radius:self.bounds.size.width*(0.225*_scale) startAngle:M_PI*0 endAngle:M_PI*2 clockwise:YES];
            
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))) radius:self.bounds.size.width*(0.45*_scale) startAngle:M_PI*1 endAngle:M_PI*2 clockwise:YES];
            
            [path appendPath:path2];
            [path stroke];
        }
            //头
//            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale)), self.bounds.size.width*(0.225*_scale), M_PI*0, M_PI*2,0);
//
//            CGContextStrokePath(context);
//
//            //身
//            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.45*_scale), M_PI*1, M_PI*2,0);
//
//            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypePlus://加法
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            //加号
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)))];
            
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path stroke];
        }
            //圆
//            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
//
//            CGContextStrokePath(context);
//
//            //加号
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeMinus://减法
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            //减号
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
            
        case WJButtonTypeMultiply://乘法
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            //乘号
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path stroke];
        }
            
            break;
            
            
        case WJButtonTypeDivision://除号
            
        {
            //圆
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:360 clockwise:YES];
            
            //圆点1
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 setLineWidth:_lineWidth<1.6?_lineWidth*2.0:_lineWidth*1.5];
            [path2 setLineCapStyle:_lineCap];
            [path2 setLineJoinStyle:_lineJoin];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)))];
            
            [path2 stroke];
            
            //圆点2
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 setLineWidth:_lineWidth<1.6?_lineWidth*2.0:_lineWidth*1.5];
            [path3 setLineCapStyle:_lineCap];
            [path3 setLineJoinStyle:_lineJoin];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)))];
            
            [path3 stroke];
            
            //线段
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path appendPath:path4];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypePercent://百分号
            
        {
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.82+(0.5-0.82)*(1.0-_scale)), self.bounds.size.height*(0.18+(0.5-0.18)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.18+(0.5-0.18)*(1.0-_scale)), self.bounds.size.height*(0.82+(0.5-0.82)*(1.0-_scale)))];
            
            //圆圈
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.26+(0.5-0.26)*(1.0-_scale)), self.bounds.size.height*(0.26+(0.5-0.26)*(1.0-_scale))) radius:self.bounds.size.height*(0.08*_scale) startAngle:0 endAngle:M_PI*2 clockwise:YES];
            
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.74+(0.5-0.74)*(1.0-_scale)), self.bounds.size.height*(0.74+(0.5-0.74)*(1.0-_scale))) radius:self.bounds.size.height*(0.08*_scale) startAngle:0 endAngle:M_PI*2 clockwise:YES];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path stroke];
            
        }
            
            break;
            
            
        case WJButtonTypeReload://加载（圆形箭头）
          
        {
            //弧形
            [path addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))) radius:self.bounds.size.width*(0.45*_scale) startAngle:0 endAngle:M_PI*(1.0+5.0/6.0) clockwise:YES];
            
            //尖头
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.height*(0.035+(0.5-0.035)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeUpdate://更新（双半圆箭头）
            
        {
            //弧形1
            [path addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))) radius:self.bounds.size.width*(0.45*_scale) startAngle:M_PI*1 endAngle:M_PI*(1.0+5.0/6.0) clockwise:YES];
            
            //尖头1
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.height*(0.035+(0.5-0.035)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.275+(0.5-0.275)*(1.0-_scale)))];
            
            //弧形2
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))) radius:self.bounds.size.width*(0.45*_scale) startAngle:M_PI*0 endAngle:M_PI*(5.0/6.0) clockwise:YES];
            
            //尖头2
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 moveToPoint:CGPointMake(self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.965+(0.5-0.965)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.725+(0.5-0.725)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.725+(0.5-0.725)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path appendPath:path4];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeDownload://下载
            
        {
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //矩形框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeUpload://上传
            
        {
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //矩形框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeRefresh://刷新（圆形无箭头）
            
        {
            //弧形
            [path addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))) radius:self.bounds.size.width*(0.45*_scale) startAngle:M_PI*0 endAngle:M_PI*(1.0+5.0/6.0) clockwise:YES];
            
            [path stroke];
        }
            break;
            
        case WJButtonTypeLocation://位置
            
        {
            //指针
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            
            [path fill];
        }
            
            break;
            
        case WJButtonTypeNearby://附近
            
        {
            //圆圈
            [path addArcWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2) radius:self.bounds.size.width/2*(0.9*_scale) startAngle:0 endAngle:M_PI*2 clockwise:YES];
            
            [path stroke];
            
            //指针
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 setLineWidth:_lineWidth];
            [path2 setLineCapStyle:_lineCap];
            [path2 setLineJoinStyle:_lineJoin];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            
            [path2 fill];
        }
            
            break;
            
            
        case WJButtonTypeCoordinate://坐标
            
        {
            //外部形状
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)))];
            [path addArcWithCenter:CGPointMake(self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))) radius:self.bounds.size.height*(0.3*_scale) startAngle:M_PI endAngle:M_PI*2 clockwise:YES];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            [path addQuadCurveToPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))) controlPoint:CGPointMake(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)))];
            
            //内小圆
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 addArcWithCenter:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))) radius:self.bounds.size.height*(0.12*_scale) startAngle:0 endAngle:M_PI*2 clockwise:YES];
            
            [path appendPath:path2];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeSend://发送
            
        {
            //纸飞机
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
            
        case WJButtonTypeMail://邮件  两种实现方式效果一致
            
        {
            //圆角矩形
            path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale))) cornerRadius:self.bounds.size.height*(0.1*_scale)];
            
            //内部线条
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.5*2.0/3.0+(0.5-0.5*2.0/3.0)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 moveToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(1-0.5*2.0/3.0+(0.5-(1.0-0.5*2.0/3.0))*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path appendPath:path4];
            [path stroke];
        }
//            //外框
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale))+self.bounds.size.height*(0.1+_scale), self.bounds.size.height*(0.1*_scale));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
//            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
//            CGContextClosePath(context);
//
//            CGContextStrokePath(context);
//
//            //内线
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5*2.0/3.0+(0.5-0.5*2.0/3.0)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
//
//            CGContextMoveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
//            CGContextAddLineToPoint(context, self.bounds.size.width*(1-0.5*2.0/3.0+(0.5-(1.0-0.5*2.0/3.0))*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
//
//            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeComment://评论
            
        {
            //气泡
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeChat://聊天
            
        {
            //气泡
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //圆圈
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale)));
            
            CGContextStrokePath(context);
            
            //圆圈
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeFocus://对焦
            
        {
            //左上角
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            
            //右上角
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            //右下角
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            //左下角
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 moveToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path appendPath:path4];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRoundedFocus://对焦（圆角）
            
        {
            //左上角
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale))), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //右上角
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //右下角
            CGContextMoveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //左下角
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
        }
            
            break;
            
        case WJButtonTypeScan://扫描
            
        {
            //左上角
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            
            //右上角
            UIBezierPath *path2 = [UIBezierPath bezierPath];
            [path2 moveToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)))];
            [path2 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)))];
            
            //右下角
            UIBezierPath *path3 = [UIBezierPath bezierPath];
            [path3 moveToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path3 addLineToPoint:CGPointMake(self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            
            //左下角
            UIBezierPath *path4 = [UIBezierPath bezierPath];
            [path4 moveToPoint:CGPointMake(self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)))];
            [path4 addLineToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)))];
            
            //横线
            UIBezierPath *path5 = [UIBezierPath bezierPath];
            [path5 moveToPoint:CGPointMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            [path5 addLineToPoint:CGPointMake(self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)))];
            
            [path appendPath:path2];
            [path appendPath:path3];
            [path appendPath:path4];
            [path appendPath:path5];
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeRoundedScan://扫描（圆角）
            
        {
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeCamera://相机
            
        {
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //镜头
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.2*_scale), M_PI*0, M_PI*2,0);
            
            CGContextStrokePath(context);
            
            //按钮
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*2.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
        }
            
            break;
            
        case WJButtonTypeCalendar://日历
            
        {
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.height*(0.15*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.15*_scale), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.height*(0.15*_scale), self.bounds.size.height*(0.15*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.15*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.15*_scale), self.bounds.size.height*(0.15*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //连接
            CGContextSetLineWidth(context, _lineWidth*2.4);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.08+(0.5-0.08)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.22+(0.5-0.22)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.08+(0.5-0.08)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.22+(0.5-0.22)*(1.0-_scale)));
            
            CGContextStrokePath(context);
        }
            
            break;
            
        case WJButtonTypeBluetooth://蓝牙
            
        {
            //线段
            [path moveToPoint:CGPointMake(self.bounds.size.width*(0.154+(0.5-0.154)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.846+(0.5-0.846)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.846+(0.5-0.846)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)))];
            [path addLineToPoint:CGPointMake(self.bounds.size.width*(0.154+(0.5-0.154)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)))];
            
            [path stroke];
        }
            
            break;
            
        case WJButtonTypeBrightness://亮度
            
        {
            //圆
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.25*_scale), M_PI*0, M_PI*2, 0);
            
            CGContextFillPath(context);
            
            //周围线段
            CGFloat cX = CGRectGetMidX(self.bounds);
            CGFloat cY = CGRectGetMidY(self.bounds);
            
            CGContextTranslateCTM(context, cX, cY);
            for (int i = 0; i < 8; i++)
            {
                CGContextAddRect(context, RECT(0, self.bounds.size.height/3*(0.9*_scale), 1.0*_scale, self.bounds.size.height/4.5*_scale));
                CGContextRotateCTM(context, 45*M_PI/180);
            }
            CGContextStrokePath(context);
        }

            break;
            
        case WJButtonTypeNight://夜晚
            
            //月亮
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4*_scale), M_PI*1.5, M_PI*0, 1);
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            
            CGContextFillPath(context);
            
            break;
            
            
        case WJButtonTypeMicphone://麦克风
            
            //底座
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.25*_scale), M_PI*1.0, M_PI*0, 1);
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //麦克风
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale), M_PI*1.0, M_PI*0, 0);
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.15*_scale), M_PI*0, M_PI*1.0, 0);
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            break;
            
            
        case WJButtonTypeFlying://飞行
            
            //机身
            CGContextAddArc(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale), M_PI*0.5, M_PI*1.5, 1);
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            //机翼
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            break;
            
            
        case WJButtonTypeHome://主页
            
            //屋顶
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //屋子
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.05*_scale), self.bounds.size.height*(0.05*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //门
            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //烟囱
            CGContextMoveToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.12+(0.5-0.12)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.12+(0.5-0.12)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeMore://更多内容
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*2.4);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*2.4*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+_lineWidth*2.4*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth*2.4*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth*2.4*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))+_lineWidth*2.4*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))+_lineWidth*2.4*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            break;
            
            
        case WJButtonTypeList://列表
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+_lineWidth*3, self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+_lineWidth*3, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+_lineWidth*3, self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*3.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+_lineWidth*3*0.5, self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+_lineWidth*3*0.5, self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+_lineWidth*3*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+_lineWidth*3*0.5, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+_lineWidth*3*0.5, self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+_lineWidth*3*0.5, self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            break;
            
            
        case WJButtonTypeClock://时钟
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth*3.0);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            //指针
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.16+(0.5-0.16)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.32+(0.5-0.32)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeDocument://文档
            
            //边框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //折角
            CGContextMoveToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeBalance://天平
            
            //架子
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //托盘
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeBooking://预约
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
            
            CGContextStrokePath(context);
            
            //刻度
            CGContextMoveToPoint(context, self.bounds.size.width*(0.12+(0.5-0.12)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.18+(0.5-0.18)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.88+(0.5-0.88)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.82+(0.5-0.82)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.12+(0.5-0.12)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.18+(0.5-0.18)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.88+(0.5-0.88)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.82+(0.5-0.82)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //指针
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCure://治疗
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
            
            CGContextStrokePath(context);
            
            //十字
            CGContextSetLineWidth(context, _lineWidth*3.0);
            CGContextSetLineCap(context, kCGLineCapSquare);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeAid://急救
            
            //箱子
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //提手
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //十字
            CGContextSetLineWidth(context, _lineWidth*2.0);
            CGContextSetLineCap(context, kCGLineCapSquare);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.325+(0.5-0.325)*(1.0-_scale)), self.bounds.size.height*(0.575+(0.5-0.575)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.675+(0.5-0.675)*(1.0-_scale)), self.bounds.size.height*(0.575+(0.5-0.575)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeMicroscope://显微镜
            
            //机身
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.35*_scale), M_PI*1.5, M_PI*1,0);
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.92+(0.5-0.92)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.92+(0.5-0.92)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //镜筒
            CGContextSetLineWidth(context, _lineWidth*3.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeWifi://Wi-Fi
            
            //弧形
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.2*_scale), M_PI*1.25, M_PI*1.75,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.4*_scale), M_PI*1.25, M_PI*1.75,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.6*_scale), M_PI*1.25, M_PI*1.75,0);
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*3.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeBroadcast://广播
            
            //弧形
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.2*_scale), M_PI*0.75, M_PI*1.25,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.325*_scale), M_PI*0.75, M_PI*1.25,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.45*_scale), M_PI*0.75, M_PI*1.25,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.2*_scale), M_PI*1.75, M_PI*0.25,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.325*_scale), M_PI*1.75, M_PI*0.25,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.45*_scale), M_PI*1.75, M_PI*0.25,0);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*3.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeBroadcast2://广播2
            
            //弧形
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.2*_scale), M_PI*0.75, M_PI*0.25,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.325*_scale), M_PI*0.75, M_PI*0.25,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.45*_scale), M_PI*0.75, M_PI*0.25,0);
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*3.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeSubscription://订阅
            
            //弧形
            CGContextSetLineWidth(context, _lineWidth*2.0);
            
            CGContextAddArc(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.35*_scale), M_PI*1.5, M_PI*2,0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.7*_scale), M_PI*1.5, M_PI*2,0);
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*3.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeNorth://北面
            
            //指针
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.069+(0.5-0.069)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.783+(0.5-0.783)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.727+(0.5-0.727)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.217+(0.5-0.217)*(1.0-_scale)), self.bounds.size.height*(0.931+(0.5-0.931)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.069+(0.5-0.069)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeArticle://文章
            
            //外框
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //折角
            CGContextMoveToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.27+(0.5-0.27)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.27+(0.5-0.27)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.72+(0.5-0.72)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.27+(0.5-0.27)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.72+(0.5-0.72)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCloud://云
            
            //云
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(1.03+(0.5-1.03)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(1.03+(0.5-1.03)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((0.8+0.23*sinf(acosf(0.941)))+(0.5-(0.8+0.23*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.23*0.941)+(0.5-(0.3-0.23*0.941))*(1.0-_scale)), self.bounds.size.width*((0.8-0.941*0.4+0.23*sinf(acosf(0.941)))+(0.5-(0.8-0.941*0.4+0.23*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941))-0.23*0.941)+(0.5-(0.3-0.4*sinf(acosf(0.941))-0.23*0.941))*(1.0-_scale)), self.bounds.size.width*((0.8-0.941*0.4)+(0.5-(0.8-0.941*0.4))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941)))+(0.5-(0.3-0.4*sinf(acosf(0.941))))*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((0.8-0.941*0.4-0.20*sinf(acosf(0.941)))+(0.5-(0.8-0.941*0.4-0.20*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941))-0.1265*0.941)+(0.5-(0.3-0.4*sinf(acosf(0.941))-0.1265*0.941))*(1.0-_scale)), self.bounds.size.width*((0.2-0.20*sinf(acosf(0.941)))+(0.5-(0.2-0.20*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.1265*0.8423)+(0.5-(0.3-0.1265*0.8423))*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((-0.03)+(0.5-(-0.03))*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*((-0.03)+(0.5-(-0.03))*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCloudDownload://云下载
            
            //云
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(1.03+(0.5-1.03)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(1.03+(0.5-1.03)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((0.8+0.23*sinf(acosf(0.941)))+(0.5-(0.8+0.23*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.23*0.941)+(0.5-(0.3-0.23*0.941))*(1.0-_scale)), self.bounds.size.width*((0.8-0.941*0.4+0.23*sinf(acosf(0.941)))+(0.5-(0.8-0.941*0.4+0.23*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941))-0.23*0.941)+(0.5-(0.3-0.4*sinf(acosf(0.941))-0.23*0.941))*(1.0-_scale)), self.bounds.size.width*((0.8-0.941*0.4)+(0.5-(0.8-0.941*0.4))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941)))+(0.5-(0.3-0.4*sinf(acosf(0.941))))*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((0.8-0.941*0.4-0.20*sinf(acosf(0.941)))+(0.5-(0.8-0.941*0.4-0.20*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941))-0.1265*0.941)+(0.5-(0.3-0.4*sinf(acosf(0.941))-0.1265*0.941))*(1.0-_scale)), self.bounds.size.width*((0.2-0.20*sinf(acosf(0.941)))+(0.5-(0.2-0.20*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.1265*0.8423)+(0.5-(0.3-0.1265*0.8423))*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((-0.03)+(0.5-(-0.03))*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*((-0.03)+(0.5-(-0.03))*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCloudUpload://云上传
            
            //云
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(1.03+(0.5-1.03)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(1.03+(0.5-1.03)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((0.8+0.23*sinf(acosf(0.941)))+(0.5-(0.8+0.23*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.23*0.941)+(0.5-(0.3-0.23*0.941))*(1.0-_scale)), self.bounds.size.width*((0.8-0.941*0.4+0.23*sinf(acosf(0.941)))+(0.5-(0.8-0.941*0.4+0.23*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941))-0.23*0.941)+(0.5-(0.3-0.4*sinf(acosf(0.941))-0.23*0.941))*(1.0-_scale)), self.bounds.size.width*((0.8-0.941*0.4)+(0.5-(0.8-0.941*0.4))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941)))+(0.5-(0.3-0.4*sinf(acosf(0.941))))*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((0.8-0.941*0.4-0.20*sinf(acosf(0.941)))+(0.5-(0.8-0.941*0.4-0.20*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.4*sinf(acosf(0.941))-0.1265*0.941)+(0.5-(0.3-0.4*sinf(acosf(0.941))-0.1265*0.941))*(1.0-_scale)), self.bounds.size.width*((0.2-0.20*sinf(acosf(0.941)))+(0.5-(0.2-0.20*sinf(acosf(0.941))))*(1.0-_scale)), self.bounds.size.height*((0.3-0.1265*0.8423)+(0.5-(0.3-0.1265*0.8423))*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*((-0.03)+(0.5-(-0.03))*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*((-0.03)+(0.5-(-0.03))*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeLock://锁
            
            //锁身
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //锁环
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.0+(0.5-0.0)*(1.0-_scale)), self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.0+(0.5-0.0)*(1.0-_scale)), self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //锁孔
            CGContextSetLineWidth(context, _lineWidth<1.6?_lineWidth*2.0:_lineWidth*1.5);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth<1.6?_lineWidth*4.0:_lineWidth*3.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeImage://图片
            
            //矩形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //山脉
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //太阳
            CGContextAddArc(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.04*_scale), M_PI*0, M_PI*2,0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeBrokenImage://破损图片
            
            //矩形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //大叉
            CGContextSetLineWidth(context, _lineWidth*2.0);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            
            break;
            
            
        case WJButtonTypeFemale://女性
            
            //圆
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.25*_scale), M_PI*0, M_PI*2,0);
            
            CGContextStrokePath(context);
            
            //十字
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale))+_lineWidth*0.5);
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeMale://男性
            
            //圆
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.25*_scale), M_PI*0, M_PI*2,0);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale))-_lineWidth*0.5);
            
            CGContextStrokePath(context);
            
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeSun://太阳
            
            //圆
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.25*_scale), M_PI*0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            //光线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.18+(0.5-0.18)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.82+(0.5-0.82)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.18+(0.5-0.18)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.82+(0.5-0.82)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.273+(0.5-0.273)*(1.0-_scale)), self.bounds.size.height*(0.273+(0.5-0.273)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.727+(0.5-0.727)*(1.0-_scale)), self.bounds.size.height*(0.727+(0.5-0.727)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.727+(0.5-0.727)*(1.0-_scale)), self.bounds.size.height*(0.273+(0.5-0.273)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.273+(0.5-0.273)*(1.0-_scale)), self.bounds.size.height*(0.727+(0.5-0.727)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeMoon://月亮
            
            //外圆
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4*_scale), M_PI*0.5-M_PI/6.0, M_PI*1.5+M_PI/6.0, 0);
            
            CGContextStrokePath(context);
            
            //内圆
            CGContextAddArc(context, self.bounds.size.width*((0.5+0.2)+(0.5-0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*0.4*(sqrtf(3.0)/2.0)*_scale, M_PI*1.5, M_PI*0.5, 1);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeLightning://闪电
            
            //闪电
            CGContextMoveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeInfomation://提示
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth*1.5);
            
            //i
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.46+(0.5-0.46)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeForbid://禁止
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
            
            CGContextStrokePath(context);
            
            //叉
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeBan://禁止
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeQuestion://问题
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.9*_scale), self.bounds.size.width*(0.9*_scale)));
            
            CGContextStrokePath(context);
            
            //问号
            CGContextSetLineWidth(context, _lineWidth*1.5);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.width*(0.15*_scale), M_PI*1.0, M_PI*2,0);
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCaution://注意
            
            //三角形
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale))+self.bounds.size.height*(0.087*_scale));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.11+(0.5-0.11)*(1.0-_scale))+self.bounds.size.height*(0.087*_scale));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale))-self.bounds.size.height*(0.087*_scale));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale))-self.bounds.size.width*(0.087*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.087*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale)), self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.width*(0.05*_scale), self.bounds.size.height*(0.89+(0.5-0.89)*(1.0-_scale))-self.bounds.size.height*(0.087*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //叹号
            CGContextSetLineWidth(context, _lineWidth*1.5);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.66+(0.5-0.66)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeTarget://目标
            
            //圆
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.28*_scale), M_PI*0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.38+(0.5-0.38)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.62+(0.5-0.62)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.38+(0.5-0.38)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.62+(0.5-0.62)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCall://呼叫
            
            //电话
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.0+(0.5-0.0)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(1.0+(0.5-1.0)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypePhone://iPhone
            
            //机身
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale))+self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale))+self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale))-self.bounds.size.width*(0.1*_scale), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.1*_scale));
            CGContextAddArcToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale))-self.bounds.size.height*(0.1*_scale), self.bounds.size.height*(0.1*_scale));
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //屏幕
            CGContextMoveToPoint(context, self.bounds.size.width*(0.26+(0.5-0.26)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.74+(0.5-0.74)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.74+(0.5-0.74)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.26+(0.5-0.26)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextClosePath(context);
            
            //            CGContextStrokePath(context);
            CGContextFillPath(context);
            
            //听筒
            CGContextMoveToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //Home键
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.05*_scale), M_PI*0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeAries://白羊座
            
            //曲线
            CGContextAddArc(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.width*(0.15*_scale), M_PI*0.5, M_PI*1.5, 0);
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.width*(0.15*_scale), M_PI*1.5, M_PI*0.5, 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeTaurus://金牛座
            
            //圆
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.width*(0.25*_scale), M_PI*0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            //曲线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //曲线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeGemini://双子座
            
            //弧
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(1.543+(0.5-1.543)*(1.0-_scale)), self.bounds.size.height*(0.8*_scale), M_PI*(1.5-1.0/6.0), M_PI*(1.5+1.0/6.0), 0);
            
            CGContextStrokePath(context);
            
            //弧
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*((-0.543)+(0.5-(-0.543))*(1.0-_scale)), self.bounds.size.height*(0.8*_scale), M_PI*(0.5-1.0/6.0), M_PI*(0.5+1.0/6.0), 0);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.293+(0.5-0.293)*(1.0-_scale)), self.bounds.size.height*(0.23+(0.5-0.23)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.293+(0.5-0.293)*(1.0-_scale)), self.bounds.size.height*(0.77+(0.5-0.77)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.707+(0.5-0.707)*(1.0-_scale)), self.bounds.size.height*(0.23+(0.5-0.23)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.707+(0.5-0.707)*(1.0-_scale)), self.bounds.size.height*(0.77+(0.5-0.77)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCancer://巨蟹座
            
            //圆
            CGContextAddArc(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.15*_scale), M_PI*0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddArc(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.width*(0.15*_scale), M_PI*0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            //曲线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.144+(0.5-0.144)*(1.0-_scale)), self.bounds.size.height*(0.294+(0.5-0.294)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.523+(0.5-0.523)*(1.0-_scale)), self.bounds.size.height*(0.0+(0.5-0.0)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.294+(0.5-0.294)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //曲线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.856+(0.5-0.856)*(1.0-_scale)), self.bounds.size.height*(0.706+(0.5-0.706)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.477+(0.5-0.477)*(1.0-_scale)), self.bounds.size.height*(1.0+(0.5-1.0)*(1.0-_scale)), self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.706+(0.5-0.706)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeLeo://狮子座
            
            //曲线
            CGContextAddArc(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.width*(0.15*_scale), M_PI*0, M_PI*2, 0);
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.2*_scale), M_PI*1, M_PI*2, 0);
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.width*(0.725+(0.5-0.725)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.width*(0.125*_scale), M_PI*1, M_PI*2, 1);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeVirgo://处女座
            
            //曲线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeLibra://天秤座
            
            //曲线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.6165+(0.5-0.6165)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.375+(0.5-0.375)*(1.0-_scale)), self.bounds.size.height*(0.6165+(0.5-0.6165)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.25*_scale), M_PI*(0.5+1.0/6.0), M_PI*(2.0+1.0/3.0), 0);
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.6165+(0.5-0.6165)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeScorpio://天蝎座
            
            //qux
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.875+(0.5-0.875)*(1.0-_scale)), self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeSagittarius://射手座
            
            //箭头
            CGContextMoveToPoint(context, self.bounds.size.width*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeCapricorn://摩羯座
            
            //曲线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.15+(0.5-0.15)*(1.0-_scale)), self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeAquarius://水瓶座
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.85+(0.5-0.85)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypePisces://双鱼座
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //曲线
            CGContextAddArc(context, self.bounds.size.width*(-0.2+(0.5-(-0.2))*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.6*_scale), M_PI*(1.0/4.0), M_PI*(2.0-1.0/4.0), 1);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(1.2+(0.5-1.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.6*_scale), M_PI*(1.0-1.0/4.0), M_PI*(1.0+1.0/4.0), 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
            
#pragma -mark 图表
            
            
        case WJButtonTypeLineChart://折线图
            
            //底线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-0.2/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+0.2/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+0.1/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-0.1/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-0.15/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+0.15/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeLineChart2://折线图2
            
            //坐标系
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*0*_scale, self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.22*_scale), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.44*_scale), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.66*_scale), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*2.4);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*0*_scale, self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*0*_scale, self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.22*_scale), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.22*_scale), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.44*_scale), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.44*_scale), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.66*_scale), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.17+(0.5-0.17)*(1.0-_scale))+self.bounds.size.width*(0.66*_scale), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeLineChart3://折线图3
            
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*0*_scale, self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.22*_scale), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.44*_scale), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.66*_scale), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.88*_scale), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextSetLineWidth(context, _lineWidth*2.4);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*0*_scale, self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*0*_scale, self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.22*_scale), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.22*_scale), self.bounds.size.height*(0.85+(0.5-0.85)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.44*_scale), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.44*_scale), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.66*_scale), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.66*_scale), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.88*_scale), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.06+(0.5-0.06)*(1.0-_scale))+self.bounds.size.width*(0.88*_scale), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeAreaChart://面积图
            
            //底线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-0.2/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+0.2/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+0.1/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-0.1/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-0.15/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+0.15/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeAreaChart2://面积图2
            
            //底线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //圆
            CGContextAddEllipseInRect(context, CGRectMake(self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale))-_lineWidth*1.3, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))-_lineWidth*1.3, _lineWidth*2.6, _lineWidth*2.6));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-0.2/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+0.2/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+0.1/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-0.1/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))+0.26/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-0.15/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+0.15/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+_lineWidth*1.3);
            
            CGContextStrokePath(context);
            
            
            //面积
            CGContextMoveToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth*1.3);
            CGContextAddArc(context, self.bounds.size.width*(0.11+(0.5-0.11)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), _lineWidth*1.3, M_PI*0.5, M_PI*2.0-acosf(0.26/(sqrtf(0.26*0.26+0.2*0.2))), 1);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3*_scale, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))+0.2/(sqrtf(0.26*0.26+0.2*0.2))*_lineWidth*1.3*_scale);
            CGContextAddArc(context, self.bounds.size.width*((0.11+0.26)+(0.5-0.11-0.26)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)), _lineWidth*1.3, M_PI*0.5+asinf(0.26/(sqrtf(0.26*0.26+0.2*0.2))), M_PI*0.5-asinf(0.26/(sqrtf(0.26*0.26+0.1*0.1))), 1);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3*_scale, self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale))-0.1/(sqrtf(0.26*0.26+0.1*0.1))*_lineWidth*1.3*_scale);
            CGContextAddArc(context, self.bounds.size.width*((0.11+0.26*2)+(0.5-0.11-0.26*2)*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)), _lineWidth*1.3, M_PI*1.0+acosf(0.26/(sqrtf(0.26*0.26+0.1*0.1))), M_PI*2.0-acosf(0.26/(sqrtf(0.26*0.26+0.15*0.15))), 1);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale))-0.26/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3*_scale, self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale))+0.15/(sqrtf(0.26*0.26+0.15*0.15))*_lineWidth*1.3*_scale);
            CGContextAddArc(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)), _lineWidth*1.3, M_PI*0.5+asinf(0.26/(sqrtf(0.26*0.26+0.15*0.15))), M_PI*0.5, 1);
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.11+0.26*3)+(0.5-0.11-0.26*3)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            
            break;
            
            
        case WJButtonTypeBarChart://条形图
            
            //底线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //条形
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+0.11)+(0.5-0.05-0.11)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11)+(0.5-0.05-0.11)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11+0.16)+(0.5-0.05-0.11-0.16)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11+0.16)+(0.5-0.05-0.11-0.16)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1)+(0.5-0.05-0.11-0.16-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1)+(0.5-0.05-0.11-0.16-0.1)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1+0.16)+(0.5-0.05-0.11-0.16-0.1-0.16)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1+0.16)+(0.5-0.05-0.11-0.16-0.1-0.16)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1)+(0.5-(0.05+(0.11+0.16)*2+0.1))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1)+(0.5-(0.05+(0.11+0.16)*2+0.1))*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1+0.16)+(0.5-(0.05+(0.11+0.16)*2+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1+0.16)+(0.5-(0.05+(0.11+0.16)*2+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeBarChart2://条形图2
            
            //坐标系
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.05+(0.5-0.05)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //条形
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+0.09)+(0.5-0.05-0.09)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.09)+(0.5-0.05-0.09)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.09+0.18)+(0.5-0.05-0.09-0.18)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.09+0.18)+(0.5-0.05-0.09-0.18)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)+0.09)+(0.5-0.05-0.09-0.18-0.09)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)+0.09)+(0.5-0.05-0.09-0.18-0.09)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)+0.09+0.18)+(0.5-0.05-0.09-0.18-0.09-0.18)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)+0.09+0.18)+(0.5-0.05-0.09-0.18-0.09-0.18)*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)*2+0.09)+(0.5-(0.05+(0.09+0.18)*2+0.09))*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)*2+0.09)+(0.5-(0.05+(0.09+0.18)*2+0.09))*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)*2+0.09+0.18)+(0.5-(0.05+(0.09+0.18)*2+0.09+0.18))*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.09+0.18)*2+0.09+0.18)+(0.5-(0.05+(0.09+0.18)*2+0.09+0.18))*(1.0-_scale)), self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeStackChart://堆图
            
            //底线
            CGContextMoveToPoint(context, self.bounds.size.width*(0.05+(0.5-0.05)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //条形
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+0.11)+(0.5-0.05-0.11)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11)+(0.5-0.05-0.11)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11+0.16)+(0.5-0.05-0.11-0.16)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11+0.16)+(0.5-0.05-0.11-0.16)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+0.11)+(0.5-0.05-0.11)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11)+(0.5-0.05-0.11)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11+0.16)+(0.5-0.05-0.11-0.16)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+0.11+0.16)+(0.5-0.05-0.11-0.16)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextFillPath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1)+(0.5-(0.05+(0.11+0.16)+0.1))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1)+(0.5-(0.05+(0.11+0.16)+0.1))*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1+0.16)+(0.5-(0.05+(0.11+0.16)+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1+0.16)+(0.5-(0.05+(0.11+0.16)+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1)+(0.5-(0.05+(0.11+0.16)+0.1))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1)+(0.5-(0.05+(0.11+0.16)+0.1))*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1+0.16)+(0.5-(0.05+(0.11+0.16)+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.55+(0.5-0.55)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)+0.1+0.16)+(0.5-(0.05+(0.11+0.16)+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextFillPath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1)+(0.5-(0.05+(0.11+0.16)*2+0.1))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1)+(0.5-(0.05+(0.11+0.16)*2+0.1))*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1+0.16)+(0.5-(0.05+(0.11+0.16)*2+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.4+(0.5-0.4)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1+0.16)+(0.5-(0.05+(0.11+0.16)*2+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1)+(0.5-(0.05+(0.11+0.16)*2+0.1))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1)+(0.5-(0.05+(0.11+0.16)*2+0.1))*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1+0.16)+(0.5-(0.05+(0.11+0.16)*2+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*((0.05+(0.11+0.16)*2+0.1+0.16)+(0.5-(0.05+(0.11+0.16)*2+0.1+0.16))*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextFillPath(context);
            
            break;
            
            
            
        case WJButtonTypePieChart://饼图
            
            //弧形
            CGContextMoveToPoint(context, self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.45*_scale), M_PI*0, M_PI*1.5,0);
            CGContextAddLineToPoint(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale)));
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.2*_scale), M_PI*1.5, M_PI*0,1);
            CGContextClosePath(context);
            
            CGContextStrokePath(context);
            
            //弧形
            CGContextMoveToPoint(context, self.bounds.size.height*(0.95+(0.5-0.95)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-_lineWidth);
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.45*_scale)-_lineWidth, M_PI*0, M_PI*1.5,1);
            CGContextAddLineToPoint(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.3+(0.5-0.3)*(1.0-_scale))-_lineWidth);
            CGContextAddArc(context, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))+_lineWidth, self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale))-_lineWidth, self.bounds.size.height*(0.2*_scale)-_lineWidth, M_PI*1.5, M_PI*0, 0);
            CGContextClosePath(context);
            
            CGContextFillPath(context);
            
            break;
            
            
            
#pragma -mark 键盘
            
            
        case WJButtonTypeKeyboard0://键盘:0
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard1://键盘:1
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard2://键盘:2
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)), self.bounds.size.width*(0.18*_scale), M_PI*1.0, M_PI*2, 0);
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.68+(0.5-0.68)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.275+(0.5-0.275)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.275+(0.5-0.275)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.725+(0.5-0.725)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard3://键盘:3
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.28+(0.5-0.28)*(1.0-_scale)), self.bounds.size.width*(0.18*_scale), M_PI*1.0, M_PI*2.5, 0);
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)), self.bounds.size.width*(0.22*_scale), M_PI*1.5, M_PI*3.0, 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard4://键盘:4
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.6+(0.5-0.6)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard5://键盘:5
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.width*(0.25*_scale), M_PI*1.0, M_PI*4.0/3.0, 1);
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard6://键盘:6
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)), self.bounds.size.width*(0.22*_scale), M_PI*1.0, M_PI*3.0, 0);
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.28+(0.5-0.28)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard7://键盘:7
            
            CGContextMoveToPoint(context, self.bounds.size.width*(0.275+(0.5-0.275)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.725+(0.5-0.725)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddQuadCurveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard8://键盘:8
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.28+(0.5-0.28)*(1.0-_scale)), self.bounds.size.width*(0.18*_scale), M_PI*0.5, M_PI*2.5, 0);
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)), self.bounds.size.width*(0.22*_scale), M_PI*1.5, M_PI*3.5, 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboard9://键盘:9
            
            CGContextAddArc(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)), self.bounds.size.width*(0.22*_scale), M_PI*0.0, M_PI*2, 0);
            CGContextAddCurveToPoint(context, self.bounds.size.width*(0.72+(0.5-0.72)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardPoint://键盘:小数点
            
            CGContextSetLineWidth(context, _lineWidth<1.6?_lineWidth*3.0:_lineWidth*2.4);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            break;
            
            
        case WJButtonTypeKeyboardEqual://键盘:等号
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.375+(0.5-0.375)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.375+(0.5-0.375)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.625+(0.5-0.625)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.625+(0.5-0.625)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardPlus://键盘:加号
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardMinus://键盘:减号
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardMultipy://键盘:乘号
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.25+(0.5-0.25)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardDivision://键盘:除号
            
            CGContextSetLineWidth(context, _lineWidth<1.6?_lineWidth*2.0:_lineWidth*1.5);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.32+(0.5-0.32)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆点
            CGContextMoveToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.68+(0.5-0.68)*(1.0-_scale)));
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, _lineWidth);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.2+(0.5-0.2)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardPower://键盘:^
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.45+(0.5-0.45)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardPercent://键盘:百分号
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.73+(0.5-0.73)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.37+(0.5-0.37)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //圆圈
            CGContextAddArc(context, self.bounds.size.width*(0.37+(0.5-0.37)*(1.0-_scale)), self.bounds.size.height*(0.18+(0.5-0.18)*(1.0-_scale)), self.bounds.size.height*(0.08*_scale), 0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            CGContextAddArc(context, self.bounds.size.width*(0.73+(0.5-0.73)*(1.0-_scale)), self.bounds.size.height*(0.82+(0.5-0.82)*(1.0-_scale)), self.bounds.size.height*(0.08*_scale), 0, M_PI*2, 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardLeftBrackets://键盘:左括号
            
            //圆弧
            CGContextAddArc(context, self.bounds.size.width*((0.5+0.4*1.414)+(0.5-0.5-0.4*1.414)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.4*1.414*_scale), M_PI*3.0/4.0, M_PI*5.0/4.0, 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardRightBrackets://键盘:右括号
            
            //圆弧
            CGContextAddArc(context, self.bounds.size.width*((0.5-0.4*1.414)+(0.5-0.5+0.4*1.414)*(1.0-_scale)), self.bounds.size.height*(0.5+(0.5-0.5)*(1.0-_scale)), self.bounds.size.width*(0.4*1.414*_scale), M_PI*7.0/4.0, M_PI*9.0/4.0, 0);
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardBlank://键盘:空格
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.1+(0.5-0.1)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.9+(0.5-0.9)*(1.0-_scale)), self.bounds.size.height*(0.75+(0.5-0.75)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardReturn://键盘:回车
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.35+(0.5-0.35)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.8+(0.5-0.8)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.6+(0.5-0.6)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.7+(0.5-0.7)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.4+(0.5-0.4)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardSlash://键盘:斜杠
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.73+(0.5-0.73)*(1.0-_scale)), self.bounds.size.height*(0.1+(0.5-0.1)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.37+(0.5-0.37)*(1.0-_scale)), self.bounds.size.height*(0.9+(0.5-0.9)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
            
        case WJButtonTypeKeyboardShape://键盘:#
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.65+(0.5-0.65)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.55+(0.5-0.55)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.45+(0.5-0.45)*(1.0-_scale)), self.bounds.size.height*(0.2+(0.5-0.2)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.35+(0.5-0.35)*(1.0-_scale)), self.bounds.size.height*(0.8+(0.5-0.8)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.3+(0.5-0.3)*(1.0-_scale)), self.bounds.size.height*(0.375+(0.5-0.375)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.75+(0.5-0.75)*(1.0-_scale)), self.bounds.size.height*(0.375+(0.5-0.375)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            //线段
            CGContextMoveToPoint(context, self.bounds.size.width*(0.25+(0.5-0.25)*(1.0-_scale)), self.bounds.size.height*(0.625+(0.5-0.625)*(1.0-_scale)));
            CGContextAddLineToPoint(context, self.bounds.size.width*(0.7+(0.5-0.7)*(1.0-_scale)), self.bounds.size.height*(0.625+(0.5-0.625)*(1.0-_scale)));
            
            CGContextStrokePath(context);
            
            break;
            
    }
    
    
}
@end
