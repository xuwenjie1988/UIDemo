//
//  ViewController.m
//  画图demo
//
//  Created by 文杰 许                       on 2018/3/21.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    UIImageView *imgV;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *img1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1125x2436"]];
    img1.frame = self.view.bounds;
//    UIImageView *img2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pic_placepic"]];
//    img2.userInteractionEnabled = YES;
//    img2.frame = self.view.bounds;
    
    [self.view addSubview:img1];
//    [self.view addSubview:img2];
    
    UIImage *image = [self createImageColor:[UIColor redColor] size:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.width)];
    imgV = [[UIImageView alloc] initWithImage:image];
    imgV.userInteractionEnabled = YES;
    imgV.frame = self.view.bounds;
    [self.view addSubview:imgV];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [imgV addGestureRecognizer:pan];
//
//    UIImage *newImage = [self scaleImage:image sclae:2.0];
//    imgV.image = newImage;
//
//    UIImage *waterImage = [self waterAtImage:newImage text:@"水印" point:CGPointMake(50, 50) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}];
//    imgV.image = waterImage;
//
//    UIImage *waterImg = [self waterAtImage:image waterImgae:[UIImage imageNamed:@"1125x2436"] rect:CGRectMake(25, 25, 50, 50)];
//    imgV.image = waterImg;
//
//    [self cutView:img1 success:^(UIImage *image) {
//        imgV.image = image;
//    }];
}

- (void)pan:(UIPanGestureRecognizer *)pan
{
    UIImageView *imageV = (UIImageView *)pan.view;
   imageV.image = [self wipeView:imageV point:[pan locationInView:imageV]  size:CGSizeMake(30, 30)];
}

- (UIImage *)wipeView:(UIView *)view
                point:(CGPoint)point
                 size:(CGSize)size
{
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ref = UIGraphicsGetCurrentContext();
    
    [view.layer renderInContext:ref];
    
    CGFloat clipX = point.x - size.width / 2;
    CGFloat clipY = point.y - size.height / 2;
    CGRect clipRect = CGRectMake(clipX, clipY, size.width, size.height);
    
    CGContextClearRect(ref, clipRect);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage *)createImageColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, size.width, size.height)];
    
    [color setFill];
    [path fill];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage *)scaleImage:(UIImage *)image sclae:(CGFloat)scale
{
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    
    CGSize size = CGSizeMake(width, height);
    
    UIGraphicsBeginImageContext(size);
    
    [image drawInRect:CGRectMake(0, 0, width, height)];
    
    UIImage *newImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImg;
}

- (UIImage *)waterAtImage:(UIImage *)image
                     text:(NSString *)text
                    point:(CGPoint)point
               attributes:(NSDictionary *)attributes
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0);
    
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    [text drawAtPoint:point withAttributes:attributes];
    
    UIImage *newImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImg;
}

- (UIImage *)waterAtImage:(UIImage *)image
               waterImgae:(UIImage *)waterImage
                     rect:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    [waterImage drawInRect:rect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)cutView:(UIView *)view success:(void(^)(UIImage *image))success
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0);
    CGContextRef ref = UIGraphicsGetCurrentContext();
    
    [view.layer renderInContext:ref];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    success(newImage);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
